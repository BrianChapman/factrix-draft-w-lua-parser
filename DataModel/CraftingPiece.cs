﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    [DebuggerDisplay("{Name} {Category} {CraftingSpeed}")]
    class CraftingPiece
    {
        public CraftingCategory Category { get; private set; }
        public string Name { get; private set; }
        public float CraftingSpeed { get; private set; }

        public CraftingPiece(CraftingCategory category, string name, float craftingSpeed)
        {
            Category = category;
            Name = name;
            CraftingSpeed = craftingSpeed;
        }

        public static CraftingCategory RecipeCatToConstCat(CraftingCategory recipeCat)
        {
            switch (recipeCat)
            {
            case CraftingCategory.AdvancedCrafting:
            case CraftingCategory.Crafting:
            case CraftingCategory.CraftingWithFluid:
                return CraftingCategory.Crafting;
            default:
                return recipeCat;
            }
        }

        public bool IsValidRecipe(Recipe recipe)
        {
            CraftingCategory recCat = RecipeCatToConstCat(recipe.Category);
            if (this.Category != recCat)
                return false;

            if (this.Category == CraftingCategory.Crafting)
            {
                if (recipe.Inputs.Length > 2 && this.Name == "assembly-machine-1")
                    return false;
            }

            if (this.Category == CraftingCategory.Delivery)
            {
                if (this.Name == "belt" && recipe.Outputs[0].Type == ItemType.Fluid)
                    return false;
                if (this.Name == "pipe" && recipe.Outputs[0].Type == ItemType.Item)
                    return false;
            }
            return true;
        }
    }


    class CraftingPieces
    {
        static Dictionary<CraftingCategory, CraftingPiece[]> _categoryToPieces = null;
        static Dictionary<string, CraftingPiece> _nameToPiece = null;

        static CraftingPiece Belt { get; set; }
        static CraftingPiece Pipe { get; set; }

        static CraftingPieces()
        {
            Belt = new CraftingPiece(CraftingCategory.Delivery, "belt", 1.0f);
            Pipe = new CraftingPiece(CraftingCategory.Delivery, "pipe", 1.0f);

        }

        static void BuildDictionaries()
        {
            List<CraftingPiece> pieces = new List<CraftingPiece>();

            pieces.Add(new CraftingPiece(CraftingCategory.Smelting, "stone-furnace", 1.25f));
            pieces.Add(new CraftingPiece(CraftingCategory.Smelting, "steel-furnace", 2.0f));
            pieces.Add(new CraftingPiece(CraftingCategory.Smelting, "electric-furnace", 2.0f));
            pieces.Add(new CraftingPiece(CraftingCategory.Crafting, "assembly-machine-1", 0.5f));
            pieces.Add(new CraftingPiece(CraftingCategory.Crafting, "assembly-machine-2", 0.75f));
            pieces.Add(new CraftingPiece(CraftingCategory.Crafting, "assembly-machine-3", 1.25f));
            pieces.Add(new CraftingPiece(CraftingCategory.Chemistry, "chemical-plant", 1.25f));
            pieces.Add(new CraftingPiece(CraftingCategory.Centrifuging, "centrifuge", 0.75f));
            pieces.Add(new CraftingPiece(CraftingCategory.OilProcessing, "oil-refinery", 1.0f));
            pieces.Add(new CraftingPiece(CraftingCategory.RocketBuilding, "rocket-silo", 1.0f));
            pieces.Add(Belt);
            pieces.Add(Pipe);

            var categoryToPieces = new Dictionary<CraftingCategory, List<CraftingPiece>>();
            foreach (var piece in pieces)
            {
                List<CraftingPiece> list = null;
                if (!categoryToPieces.TryGetValue(piece.Category, out list))
                {
                    list = new List<CraftingPiece>();
                    categoryToPieces.Add(piece.Category, list);
                }
                list.Add(piece);
            }


            _categoryToPieces = new Dictionary<CraftingCategory, CraftingPiece[]>();
            _nameToPiece = new Dictionary<string, CraftingPiece>();

            foreach (var list in categoryToPieces.Values)
                _categoryToPieces.Add(list[0].Category, list.ToArray());

            foreach (var piece in pieces)
                _nameToPiece.Add(piece.Name, piece);
        }

        public static CraftingPiece GetPieceByName(string name)
        {
            BuildDictionaries();
            return _nameToPiece[name];
        }

        public static CraftingPiece[] GetPiecesByCategory(CraftingCategory category)
        {
            BuildDictionaries();
            CraftingCategory constCat = CraftingPiece.RecipeCatToConstCat(category);
            return _categoryToPieces[constCat];
        }

        public static CraftingPiece GetDeliveryPiece(ItemType type)
        {
            if (type == ItemType.Fluid)
                return Pipe;
            else
                return Belt;
        }
    }
}
