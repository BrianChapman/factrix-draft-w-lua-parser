﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    class CraftingRecipe : INotifyPropertyChanged
    {
        public Recipe Recipe { get; private set; }

        public CraftingRecipe(Recipe recipe)
        {
            Recipe = recipe;
            Crafter = MinimalCrafterFor(recipe);
            NumberOfPieces = 1;
        }

        public CraftingRecipe(Recipe recipe, CraftingPiece crafter)
        {
            Recipe = recipe;

            // ensure the crafter is valid for the recipe.
            CraftingPiece[] pieces = CraftingPieces.GetPiecesByCategory(recipe.Category);
            //Debug.Assert(pieces.Contains(crafter));
            Crafter = crafter;
        }

        public CraftingRecipe(ItemAmount amt, CraftingPiece crafter)
        {
            var inputs = new List<ItemAmount>();
            var outputs = new List<ItemAmount>();
            outputs.Add(amt);
            Recipe = new Recipe(amt.Name, CraftingCategory.Delivery, 1, inputs, outputs);
            Crafter = crafter;
            NumberOfPieces = 1;
        }

        public CraftingRecipe(CraftingRecipe cr)
        {
            Recipe = cr.Recipe;
            Crafter = cr.Crafter;
            NumberOfPieces = cr.NumberOfPieces;
        }

        private CraftingPiece _crafter;
        public CraftingPiece Crafter
        {
            get { return _crafter; }
            set
            {
                _crafter = value;
                NotifyChanged("Crafter");
            }
        }

        public int _numberOfPieces;
        public int NumberOfPieces
        {
            get { return _numberOfPieces; }
            set
            {
                _numberOfPieces = value;
                NotifyChanged("NumberOfPieces");
            }
        }

        private CraftingPiece MinimalCrafterFor(Recipe recipe)
        {
            CraftingPiece[] pieces = CraftingPieces.GetPiecesByCategory(recipe.Category);
            foreach (var crafter in pieces)
            {
                if (crafter.IsValidRecipe(recipe))
                    return crafter;
            }
            Debug.Assert(false, string.Format("Valid minimal crafter not found for recipe {0}", recipe.Name));
            return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyChanged(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

}
