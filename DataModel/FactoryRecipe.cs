﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    class FactoryRecipe
    {
        private Recipe[] _recipes;
        private RecipeBook _book;

        public FactoryRecipe(Recipe recipe, RecipeBook book)
        {
            _book = book;
            List<Recipe> recipeList = new List<Recipe>();
            recipeList.Add(recipe);
            CloseRecipeList(recipeList);
            SortRecipes(recipeList);
            _recipes = recipeList.ToArray();
        }

        public Recipe[] SortedRecipes { get { return _recipes; } }

        public int RecipeLength { get { return _recipes.Length; } }

        public int ItemLength { get { return ItemHeaders.Length; } }

        private string[] _itemHeaders;
        public string[] ItemHeaders
        {
            get
            {
                if (_itemHeaders == null)
                {
                    List<string> itemsNames = new List<string>();
                    foreach (var r in _recipes)
                    {
                        foreach (var amt in r.Outputs)
                        {
                            if (!itemsNames.Contains(amt.Name))
                                itemsNames.Add(amt.Name);
                        }
                        foreach (var amt in r.Outputs)
                        {
                            if (!itemsNames.Contains(amt.Name))
                                itemsNames.Add(amt.Name);
                        }
                    }
                    _itemHeaders = itemsNames.ToArray();
                }
                return _itemHeaders;
            }
        }

        private string[] _recipeHeaders;
        public string[] RecipeHeaders
        {
            get
            {
                if (_recipeHeaders == null)
                {
                    List<string> recipeHeaders = new List<string>();
                    foreach (var r in _recipes)
                    {
                        recipeHeaders.Add(r.Name);
                    }
                    _recipeHeaders = recipeHeaders.ToArray();
                }
                return _recipeHeaders;
            }
        }

        private void CloseRecipeList(List<Recipe> recipeList)
        {
            HashSet<string> provided = new HashSet<string>();
            HashSet<string> needed = new HashSet<string>();
            do
            {
                provided.Clear();
                needed.Clear();
                foreach (var r in recipeList)
                {
                    foreach (var amt in r.Inputs)
                        needed.Add(amt.Name);
                    foreach (var amt in r.Outputs)
                        provided.Add(amt.Name);
                }
                foreach (string p in provided)
                    needed.Remove(p);
                foreach (string n in needed)
                {
                    List<Recipe> rlist = _book.GetRecipesWithOutputItem(n);
                    recipeList.Add(rlist[0]);
                }
            }
            while (needed.Count > 0);
        }

        public void AddRecipe(Recipe r)
        {
            List<Recipe> recipeList = new List<Recipe>();
            recipeList.AddRange(_recipes);
            recipeList.Add(r);
            CloseRecipeList(recipeList);
            SortRecipes(recipeList);
            _recipes = recipeList.ToArray();
            ClearCaches();
        }

        private void ClearCaches()
        {
            _recipeHeaders = null;
            _itemHeaders = null;
        }

        private void SortRecipes(List<Recipe> list)
        {
            var lessList = new List<Recipe>();

            int cnt = list.Count;
            int top = 0;

            // find the simple items first
            for (int i = 0; i < cnt; i++)
            {
                Recipe r = list[i];
                if (r.Inputs == null || r.Inputs.Length == 0)
                {
                    lessList.Add(r);
                    SwapRecipes(top, i, list);
                    top += 1;
                }
            }

            while (top < cnt)
            {
                for (int k = top; k < cnt; k++)
                {
                    if (AllInputsAreLess(list[k], lessList))
                    {
                        lessList.Add(list[k]);
                        SwapRecipes(top, k, list);
                        top += 1;
                        if (top >= cnt)
                            break;
                    }
                }
            }
            list.Reverse();
        }

        private void SwapRecipes(int a, int b, List<Recipe> list)
        {
            Recipe temp = list[a];
            list[a] = list[b];
            list[b] = temp;
        }

        private bool AllInputsAreLess(Recipe r, List<Recipe> lessRecipeList)
        {
            foreach (var amt in r.Inputs)
            {
                bool found = false;
                foreach (var lessRecipe in lessRecipeList)
                {
                    foreach (var lessAmt in lessRecipe.Outputs)
                    {
                        if (lessAmt.Name == amt.Name)
                            found = true;
                    }
                    if (found)
                        break;
                }
                if (!found)
                    return false;
            }
            return true;
        }
    }
}
