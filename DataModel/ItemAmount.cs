﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    enum ItemType { Item, Fluid }

    [DebuggerDisplay("{Amount} {Name}")]
    class ItemAmount
    {
        public ItemAmount(string name, float amount, ItemType type, float probability)
        {
            Name = name;
            Amount = amount;
            Type = type;
            Probability = probability;
        }

        public ItemType Type { get; private set; }
        public string Name { get; private set; }
        public float Amount { get; private set; }
        public float Probability { get; private set; }

    }
}
