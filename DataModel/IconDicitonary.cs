﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    class IconBook
    {
        private Dictionary<string, IconDefinition> _iconBook;
        public IconBook(List<IconDefinition> iconList)
        {
            _iconBook = new Dictionary<string, IconDefinition>();
            foreach (IconDefinition iconDef in iconList)
            {
                _iconBook.Add(iconDef.Name, iconDef);
            }
            PrepareBook();
        }

        private void PrepareBook()
        {

        }
    }
}
