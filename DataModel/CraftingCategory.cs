﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    enum CraftingCategory
    {
        Delivery,
        Smelting, OilProcessing, Chemistry,
        Crafting, CraftingWithFluid, AdvancedCrafting,
        RocketBuilding, Centrifuging
    }

    static class CraftingCategories
    {
        public static CraftingCategory Parse(string s)
        {
            switch (s)
            {
            case "smelting":
                return CraftingCategory.Smelting;

            case "oil-processing":
                return CraftingCategory.OilProcessing;

            case "chemistry":
                return CraftingCategory.Chemistry;

            case "crafting":
                return CraftingCategory.Crafting;

            case "crafting-with-fluid":
                return CraftingCategory.CraftingWithFluid;

            case "advanced-crafting":
                return CraftingCategory.AdvancedCrafting;

            case "rocket-building":
                return CraftingCategory.RocketBuilding;

            case "centrifuging":
                return CraftingCategory.Centrifuging;

            default:
                throw new FormatException(string.Format("{0} is not a valid ConstuctionCategory", s));
            }
        }
    }
}
