﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    [DebuggerDisplay("\"{Name}\": {DebugOutputList} from {DebugInputList}")]
    class Recipe
    {
        public Recipe(string name, CraftingCategory category, float energy,
                                    List<ItemAmount> inputs, List<ItemAmount> outputs)
        {
            Name = name;
            Category = category;
            EnergyRequired = energy;
            Inputs = inputs.ToArray();
            Outputs = outputs.ToArray();
        }

        public string Name { get; private set; }
        public CraftingCategory Category { get; private set; }
        public float EnergyRequired { get; private set; }
        public ItemAmount[] Outputs { get; private set; }
        public ItemAmount[] Inputs { get; private set; }

        //public ItemAmount Output { get { return Outputs[0]; } }

        private string DebugInputList { get { return StringizeList(Inputs); } }
        private string DebugOutputList { get { return StringizeList(Outputs); } }
        private string StringizeList(ItemAmount[] list)
        {
            string str = String.Empty;
            foreach (var amt in list)
                str += string.Format("{0}:{1} ", amt.Amount, amt.Name);
            return str;
        }
    }

    [DebuggerDisplay("{Normal.Name}")]
    class RecipeGroup
    {
        public Recipe Normal { get; private set; }
        public Recipe Expensive { get; private set; }

        public RecipeGroup(Recipe normal, Recipe expensive)
        {
            Normal = normal;
            Expensive = expensive;
        }

    }

}
