﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    class ProductionRecipe : INotifyPropertyChanged
    {
        FactoryRecipe _factoryRecipe;

        public ProductionRecipe(FactoryRecipe factoryRecipes)
        {
            _factoryRecipe = factoryRecipes;
        }

        public int Count { get { return _craftingRecipes.Count; } }

        private List<CraftingRecipe> _craftingRecipes = null;
        public List<CraftingRecipe> CraftingRecipes
        {
            get
            {
                if (_craftingRecipes == null)
                {
                    _craftingRecipes = new List<CraftingRecipe>();
                    foreach (var r in _factoryRecipe.SortedRecipes)
                    {
                        _craftingRecipes.Add(new CraftingRecipe(r));
                    }
                }
                return _craftingRecipes;
            }
        }

        public void UpdateList(FactoryRecipe factoryRecipe)
        {
            List<CraftingRecipe> craftingRecipes = new List<CraftingRecipe>();
            foreach (var r in factoryRecipe.SortedRecipes)
            {
                bool found = false;
                foreach (var cr in _craftingRecipes)
                {
                    if (cr.Recipe == r)
                    {
                        found = true;
                        craftingRecipes.Add(cr);
                    }
                }
                if (!found)
                    craftingRecipes.Add(new CraftingRecipe(r));
            }
            _craftingRecipes = craftingRecipes;
            ClearCaches();
        }

        private List<float> _craftingRecipeEnergy;
        public List<float> CraftingRecipeEnergy
        {
            get
            {
                if (_craftingRecipeEnergy == null)
                {
                    _craftingRecipeEnergy = new List<float>();
                    foreach (var cr in CraftingRecipes)
                    {
                        float energy = cr.Recipe.EnergyRequired;
                        _craftingRecipeEnergy.Add(energy);
                    }
                }
                return _craftingRecipeEnergy;
            }
        }

        private string[] _itemHeaders;
        public string[] ItemHeaders
        {
            get
            {
                if (_itemHeaders == null)
                {
                    HashSet<string> itemNames = new HashSet<string>();
                    foreach (var cr in CraftingRecipes)
                    {
                        var r = cr.Recipe;
                        foreach (var amt in r.Outputs)
                            itemNames.Add(amt.Name);
                        foreach (var amt in r.Outputs)
                            itemNames.Add(amt.Name);
                    }
                    _itemHeaders = itemNames.ToArray();
                }
                return _itemHeaders;
            }
        }



        public int ItemLength { get { return ItemHeaders.Length; } }

        private string[] _productionRecipeHeaders;
        public string[] ProductionRecipeHeaders
        {
            get
            {
                if (_productionRecipeHeaders == null)
                {
                    List<string> recipeHeaders = new List<string>();
                    foreach (var cr in CraftingRecipes)
                    {
                        recipeHeaders.Add(cr.Recipe.Name);
                    }
                    _productionRecipeHeaders = recipeHeaders.ToArray();
                }
                return _productionRecipeHeaders;
            }
        }

        private void ClearCaches()
        {
            _craftingRecipeEnergy = null;
            _itemHeaders = null;
            _productionRecipeHeaders = null;
        }

        public void DeleteCraftingRecipe(int index)
        {
            _craftingRecipes.RemoveAt(index);
            ClearCaches();
        }

        public void DuplicateCraftingRecipe(int index)
        {
            var cr = _craftingRecipes[index];
            var new_cr = new CraftingRecipe(cr);
            _craftingRecipes.Insert(index, new_cr);
            ClearCaches();
        }

        public void MoveRecipe(int index, int delta)
        {
            if (index + delta < 0 || index + delta >= _craftingRecipes.Count)
                return;

            var cr = _craftingRecipes[index];
            _craftingRecipes.RemoveAt(index);
            _craftingRecipes.Insert(index + delta, cr);
            ClearCaches();
        }

        public void AddCrafter(CraftingRecipe cr)
        {

        }

        private void InsertSort(CraftingRecipe cr)
        {
            int last = _craftingRecipes.Count;
            _craftingRecipes.Add(cr);

            var produced = new HashSet<string>();
            for (int i = last - 1; i >= 0; --i)
            {
                if (AreAllInputsAreInSet(cr.Recipe, produced))
                {
                    var temp = _craftingRecipes[i];
                    _craftingRecipes[i] = _craftingRecipes[last];
                    _craftingRecipes[last] = temp;
                }
                foreach (var amt in _craftingRecipes[i].Recipe.Outputs)
                    produced.Add(amt.Name);
            }
        }

        public void ConvertToDelivery(CraftingRecipe cr, RecipeBook book)
        {
            int index = _craftingRecipes.IndexOf(cr);
            var amount = cr.Recipe.Outputs[0];
            var crafter = CraftingPieces.GetDeliveryPiece(amount.Type);
            //var rr = book.GetRecipe(itemAmount.Name);
            var single = new ItemAmount(amount.Name, 1, amount.Type, 1);
            var new_cr = new CraftingRecipe(single, crafter);
            _craftingRecipes[index] = new_cr;
            ClearCaches();
        }

        private bool AreAllInputsAreInSet(Recipe recipe, HashSet<string> produced)
        {
            foreach (var amt in recipe.Inputs)
            {
                if (!produced.Contains(amt.Name))
                    return false;
            }
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyChange(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

    }
}
