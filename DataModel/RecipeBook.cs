﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    class RecipeBook
    {
        private Dictionary<string, Recipe> _recipeBook;
        public RecipeBook(Dictionary<string, Recipe> recipes)
        {
            _recipeBook = recipes;
            PrepareBook();
        }

        private string[] _recipeNames;
        public string[] Names
        {
            get
            {
                if (_recipeNames == null)
                {
                    _recipeNames = _recipeBook.Keys.ToArray();
                    Array.Sort(_recipeNames);
                }
                return _recipeNames;
            }
        }

        public Recipe GetRecipe(string recipeName)
        {
            Recipe r;
            if (!_recipeBook.TryGetValue(recipeName, out r))
                return null;
            return r;
        }

        Dictionary<string, List<Recipe>> _recipesWithOutputItem;
        public List<Recipe> GetRecipesWithOutputItem(string itemName)
        {
            if (_recipesWithOutputItem == null)
            {
                _recipesWithOutputItem = BuildRecipesWithOutputItemLookup();
            }
            List<Recipe> list;
            if (_recipesWithOutputItem.TryGetValue(itemName, out list))
                return list;
            return new List<Recipe>();
        }

        private void PrepareBook()
        {
            HashSet<string> allInputs = new HashSet<string>();
            HashSet<string> allOutputs = new HashSet<string>();
            foreach (var r in _recipeBook.Values)
            {
                foreach (var amt in r.Inputs)
                {
                    allInputs.Add(amt.Name);
                }
                foreach (var amt in r.Outputs)
                {
                    allOutputs.Add(amt.Name);
                }
            }

            // compute the set of inputs with no recipes to produce them.
            HashSet<string> baseInputs = allInputs;
            foreach (string item in allOutputs)
                baseInputs.Remove(item);

            // Invent and Add recipes to produce "iron-ore", etc from nothing.
            foreach (string item in baseInputs)
            {
                var inputs = new List<ItemAmount>();
                var outputs = new List<ItemAmount>();
                ItemType type = IsFluid(item) ? ItemType.Fluid : ItemType.Item;
                var amount = new ItemAmount(item, 1, type, 1);
                outputs.Add(amount);
                Recipe r = new Recipe(item, CraftingCategory.Delivery, 1, inputs, outputs);
                _recipeBook.Add(item, r);
            }
        }

        public bool IsFluid(string itemName)
        {
            return FluidSet.Contains(itemName);
        }

        private HashSet<string> _fluidSet;
        private HashSet<string> FluidSet
        {
            get
            {
                if (_fluidSet == null)
                {
                    // Build Collection of all the "Fluid" items.
                    // All others are "Items".
                    _fluidSet = new HashSet<string>();
                    foreach (var r in _recipeBook.Values)
                    {
                        foreach (var amt in r.Inputs)
                        {
                            if (amt.Type == ItemType.Fluid)
                                _fluidSet.Add(amt.Name);
                        }
                        foreach (var amt in r.Outputs)
                        {
                            if (amt.Type == ItemType.Fluid)
                                _fluidSet.Add(amt.Name);
                        }
                    }
                }
                return _fluidSet;
            }
        }

        private Dictionary<string, List<Recipe>> BuildRecipesWithOutputItemLookup()
        {
            var table = new Dictionary<string, List<Recipe>>();
            foreach (Recipe r in _recipeBook.Values)
            {
                foreach (var amt in r.Outputs)
                {
                    List<Recipe> entry;
                    if (!table.TryGetValue(amt.Name, out entry))
                    {
                        entry = new List<Recipe>();
                        table.Add(amt.Name, entry);
                    }
                    entry.Add(r);
                }
            }
            return table;
        }

    }

}
