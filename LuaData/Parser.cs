﻿using System;

/*
File := "data" COLON "extend" OPENPARAN Value CLOSEPARAN
Value := Bool | Number | String | List
List := OPENBRACE (ValueList | PropertyList | List) CLOSEBRACE
ValueList = Value (COMMA Value)* COMMA?
Property := Identifier EQUALS Value
PropertyList := Property (COMMA Property)*

Tokens
String = QUOTE [^QUOTE]* QUOTE
Bool = "true" | "false"
Number = digit+ (DOT digit+)?
Identifier := Letter (Letter | Digit | '_')*

Comments := "--" [.]* ENDOFLINE
*/


namespace LuaData
{
    class Parser
    {
        Tokenizer m_tokenizer;

        public Parser(string text)
        {
            m_tokenizer = new Tokenizer(text);
        }

        private void MustMatch(Token t, TokenType type, string data = "")
        {
            if (t.Type != type)
                throw new InvalidOperationException(
                    string.Format("{0} MustMatch Type failed.  Have '{1}', want '{2}'",
                                LineInfoString(t), t.Type, type));
            if (!string.IsNullOrEmpty(data) && t.Data != data)
                throw new InvalidOperationException(
                    string.Format("{0} MustMatch Data failed.  Have '{1}', want '{2}'",
                                LineInfoString(t), t.Data, data));
        }

        private string LineInfoString(Token t)
        {
            return string.Format("({0},{1})", t.Line, t.Column);
        }

        // File := "data" COLON "extend" OPENPARAN Value CLOSEPARAN
        public Value Parse()
        {
            Token t;
            t = m_tokenizer.NextToken();
            MustMatch(t, TokenType.Identifier, "data");

            t = m_tokenizer.NextEveryToken();
            MustMatch(t, TokenType.Colon);

            t = m_tokenizer.NextEveryToken();
            MustMatch(t, TokenType.Identifier, "extend");

            t = m_tokenizer.NextToken();
            MustMatch(t, TokenType.OpenParen);

            Value list = Parse_Value();

            t = m_tokenizer.NextToken();
            MustMatch(t, TokenType.CloseParen);

            return list;
        }

        private Value Parse_Value()
        {
            Token t = m_tokenizer.NextToken();
            return Parse_Value(t);
        }

        // Value := Bool, Number, String, List
        private Value Parse_Value(Token t)
        {
            switch (t.Type)
            {
            case TokenType.True:
            case TokenType.False:
                return NewBoolValue(t);

            case TokenType.Number:
                return NewNumberValue(t);

            case TokenType.String:
                return NewStringValue(t);

            case TokenType.OpenBracket:
                return Parse_List(t);
            }
            throw new InvalidOperationException(string.Format("Bad Case in Parse_Value {0}", LineInfoString(t)));
        }

        private Value NewStringValue(Token t)
        {
            return new Value(t.Data, t.Line);
        }

        private Value NewBoolValue(Token t)
        {
            if (t.Type == TokenType.True)
                return new Value(true, t.Line);
            if (t.Type == TokenType.False)
                return new Value(false, t.Line);
            throw new InvalidOperationException(string.Format("Bad Bool Case {0}", LineInfoString(t)));
        }

        private Value NewNumberValue(Token t)
        {
            float val;
            if (float.TryParse(t.Data, out val))
                return new Value(val, t.Line);
            throw new InvalidOperationException(string.Format("{0} Number conversion of '{0}' failed",
                                                            LineInfoString(t), t.Data));
        }

        // List := OPENBRACE(ValueList | PropertyList | List) CLOSEBRACE
        private Value Parse_List(Token t)
        {
            MustMatch(t, TokenType.OpenBracket);

            t = m_tokenizer.NextToken();
            switch (t.Type)
            {
            case TokenType.Identifier:
                return Parse_PropertyList(t);

            case TokenType.String:
            case TokenType.Number:
            case TokenType.True:
            case TokenType.False:
            case TokenType.OpenBracket:
                return Parse_ValueList(t);
            }
            throw new InvalidOperationException(string.Format("Bad Case in Parse_List {0}", LineInfoString(t)));
        }

        // PropertyList := Property(COMMA Property)*
        private Value Parse_PropertyList(Token t)
        {
            MustMatch(t, TokenType.Identifier);
            PropertyBag bag = new PropertyBag();

            while (true)
            {
                Property prop = Parse_Property(t);
                bag.Add(prop.Name, prop.Value);

                t = m_tokenizer.NextToken();
                if (t.Type == TokenType.CloseBracket)
                    break;
                MustMatch(t, TokenType.Comma);

                // Optional Comma
                t = m_tokenizer.NextToken();
                if (t.Type == TokenType.CloseBracket)
                    break;
                MustMatch(t, TokenType.Identifier);
            }
            return new Value(bag, t.Line);
        }

        // Property := Identifier EQUALS Value
        private Property Parse_Property(Token t)
        {
            MustMatch(t, TokenType.Identifier);
            string name = t.Data;

            t = m_tokenizer.NextToken();
            MustMatch(t, TokenType.Equals);

            Value value = Parse_Value();

            return new Property(name, value);
        }

        // ValueList = Value(COMMA Value)* COMMA?
        private Value Parse_ValueList(Token t)
        {
            ValueList list = new ValueList();
            while (true)
            {
                Value value = Parse_Value(t);
                list.Add(value);

                t = m_tokenizer.NextToken();
                if (t.Type == TokenType.CloseBracket)
                    break;
                MustMatch(t, TokenType.Comma);

                // Optional Comma
                t = m_tokenizer.NextToken();
                if (t.Type == TokenType.CloseBracket)
                    break;
            }
            return new Value(list, t.Line);
        }
    }
}
