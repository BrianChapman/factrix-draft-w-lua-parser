﻿using System;
using System.Text;

namespace LuaData
{
    class Tokenizer
    {
        string _text;
        int _pos;
        int _end;
        int _line;
        int _column;

        public Tokenizer(string text)
        {
            _text = text;
            _pos = 0;
            _end = _text.Length;
            _line = 1;
            _column = 1;
        }

        private Char PeekChar
        {
            get { return _text[_pos]; }
        }

        private Char ReadChar()
        {
            Char ch = _text[_pos++];
            if (ch != '\n')
            {
                _column += 1;
                return ch;
            }
            _line += 1;
            _column = 1;
            return ch;
        }

        public int LineNumber { get { return _line; } }
        public int ColumnNumber { get { return _column; } }

        public Token NextToken()
        {
            Token t;
            do
            {
                t = NextEveryToken();
            } while (t.Type == TokenType.Whitespace || t.Type == TokenType.Comment);
            return t;
        }

        public Token NextEveryToken()
        {
            Char ch = ReadChar();
            if (Char.IsLetter(ch))
                return ScanIndentifierOrKeyword(ch);

            if (Char.IsDigit(ch))
                return ScanNumber(ch);

            switch (ch)
            {
            case ' ':
            case '\r':
            case '\n':
            case '\t':
                return ScanWhitespace(ch);

            case '"':
                return ScanQuotedString(ch);

            case '{':
                return NewToken(TokenType.OpenBracket);
            case '}':
                return NewToken(TokenType.CloseBracket);
            case '(':
                return NewToken(TokenType.OpenParen);
            case ')':
                return NewToken(TokenType.CloseParen);
            case ':':
                return NewToken(TokenType.Colon);
            case ',':
                return NewToken(TokenType.Comma);
            case '=':
                return NewToken(TokenType.Equals);

            case '-':
                if ('-' == PeekChar)
                    return ScanLineComment(ch);
                else
                    return ScanNumber(ch);
            }
            throw new InvalidOperationException(string.Format("Unexpected character '{0}' == {1}", ch, (int)ch));
        }

        private Token NewToken(TokenType tokenType, string data = "")
        {
            return new Token(this, tokenType, data);
        }

        private Token ScanLineComment(Char ch)
        {
            while (ch != '\n')
                ch = ReadChar();
            return NewToken(TokenType.Comment);
        }

        private Token ScanQuotedString(Char ch)
        {
            if (ch != '"')
                throw new InvalidOperationException("missing start quote");

            StringBuilder sb = new StringBuilder();
            while ((ch = ReadChar()) != '"')
                sb.Append(ch);

            return NewToken(TokenType.String, sb.ToString());
        }

        private Token ScanWhitespace(char ch)
        {
            if (!Char.IsWhiteSpace(ch))
                throw new InvalidOperationException("first whitespace character should be whitespace");
            while (Char.IsWhiteSpace(PeekChar))
                ReadChar();
            return NewToken(TokenType.Whitespace);
        }

        private Token ScanIndentifierOrKeyword(char ch)
        {
            StringBuilder sb = new StringBuilder();

            if (!Char.IsLetter(ch) && ch != '_')
                throw new InvalidOperationException("identifiers must start with letter or underscore.");
            sb.Append(ch);

            while (true)
            {
                ch = PeekChar;
                if (Char.IsLetter(ch) || ch == '_')
                    sb.Append(ReadChar());
                else
                    break;
            }
            string word = sb.ToString();
            if (word == "true")
                return NewToken(TokenType.True);
            if (word == "false")
                return NewToken(TokenType.False);
            return NewToken(TokenType.Identifier, word);
        }

        private Token ScanNumber(char ch)
        {
            StringBuilder sb = new StringBuilder();

            if (!Char.IsDigit(ch) && ch != '-')
                throw new InvalidOperationException("Numbers must start with digit (or '-').");
            sb.Append(ch);
            while (true)
            {
                ch = PeekChar;
                if (Char.IsDigit(ch) || ch == '.')
                    sb.Append(ReadChar());
                else
                    break;
            }
            return NewToken(TokenType.Number, sb.ToString());
        }
    }
}
