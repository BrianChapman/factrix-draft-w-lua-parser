﻿using System.Diagnostics;

namespace LuaData
{
    enum TokenType
    {
        OpenParen, CloseParen, OpenBracket, CloseBracket,
        Colon, Comma, Equals,
        True, False,
        String, Identifier, Number,
        Whitespace, Comment
    };

    [DebuggerDisplay("{Type.ToString()}")]
    class Token
    {
        public Token(Tokenizer lineInfo, TokenType tokenType, string data = "")
        {
            Type = tokenType;
            Data = data;
            Line = lineInfo.LineNumber;
            Column = lineInfo.ColumnNumber;
        }

        public TokenType Type { get; private set; }
        public string Data { get; private set; }
        public int Line { get; private set; }
        public int Column { get; private set; }
    }
}
