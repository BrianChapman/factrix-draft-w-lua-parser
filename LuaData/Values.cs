﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LuaData
{
    class ValueList : List<Value> { }

    [DebuggerDisplay("{DebugToString()}")]
    class Value
    {
        public Values Type { get; private set; }
        public int Line { get; private set; }

        public Value(string s, int line)
        {
            Type = Values.String;
            _string = s;
            Line = line;
        }

        public Value(float number, int line)
        {
            Type = Values.Number;
            _number = number;
            Line = line;
        }

        public Value(bool b, int line)
        {
            Type = Values.Bool;
            _bool = b;
            Line = line;
        }

        public Value(ValueList value, int line)
        {
            Type = Values.ValueList;
            _valueList = value;
            Line = line;
        }

        public Value(PropertyBag bag, int line)
        {
            Type = Values.PropertyBag;
            _propertyBag = bag;
            Line = line;
        }

        string _string;
        public string String
        {
            get
            {
                if (Type != Values.String)
                    throw new InvalidOperationException(string.Format("Misread of a 'String' value. Actual type is {0}", Type.ToString()));
                return _string;
            }
            private set { _string = value; }
        }

        float _number;
        public float Number
        {
            get
            {
                if (Type != Values.Number)
                    throw new InvalidOperationException(string.Format("Misread of a 'Number' value. Actual type is {0}", Type.ToString()));
                return _number;
            }
            private set { _number = value; }
        }

        bool _bool;
        public bool Bool
        {
            get
            {
                if (Type != Values.Bool)
                    throw new InvalidOperationException(string.Format("Misread of a 'Bool' value. Actual type is {0}", Type.ToString()));
                return _bool;
            }
            private set { _bool = value; }
        }

        ValueList _valueList;
        public ValueList ValueList
        {
            get
            {
                if (Type != Values.ValueList)
                    throw new InvalidOperationException(string.Format("Misread of a 'ValueList' value. Actual type is {0}", Type.ToString()));
                return _valueList;
            }
            private set { _valueList = value; }
        }

        PropertyBag _propertyBag;
        public PropertyBag PropertyBag
        {
            get
            {
                if (Type != Values.PropertyBag)
                    throw new InvalidOperationException(string.Format("Misread of a 'PropertyList' value. Actual type is {0}", Type.ToString()));
                return _propertyBag;
            }
            private set { _propertyBag = value; }
        }

        string DebugToString()
        {
            switch (Type)
            {
            case Values.Bool:
                return _bool.ToString();
            case Values.Number:
                return _number.ToString();
            case Values.String:
                return _string;
            case Values.PropertyBag:
                return string.Format("{0} Properties", _propertyBag.Count);
            case Values.ValueList:
                return string.Format("{0} Values", _valueList.Count);
            default:
                return "?? huh ??";
            }
        }
    }
}
