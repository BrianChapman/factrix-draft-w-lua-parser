﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LuaData
{
    enum Values { String, Number, Bool, ValueList, PropertyBag };

    [DebuggerDisplay("{Name}={Value.ToString()}")]
    class Property
    {
        public string Name { get; private set; }
        public Value Value { get; private set; }

        public Property(string name, Value value)
        {
            Name = name;
            Value = value;
        }
    }


    class PropertyBag : Dictionary<string, Value>
    {
        public Value GetValue(string key)
        {
            Value value;
            if (!TryGetValue(key, out value))
                throw new InvalidOperationException(string.Format("Key {0} missing from PropertyBag", key));
            return value;
        }

        public string GetString(string key)
        {
            return GetValue(key).String;
        }
        public string GetString(string key, string def)
        {
            if (!ContainsKey(key))
                return def;
            return GetValue(key).String;
        }

        public float GetNumber(string key)
        {
            return GetValue(key).Number;
        }
        public float GetNumber(string key, float def)
        {
            if (!ContainsKey(key))
                return def;
            return GetValue(key).Number;
        }

        public ValueList GetValueList(string key)
        {
            return GetValue(key).ValueList;
        }
    }



}
