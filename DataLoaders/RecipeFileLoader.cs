﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    static class RecipeFileLoader
    {
        public static RecipeBook Load(string folderPath = null)
        {
            if (string.IsNullOrEmpty(folderPath))
                folderPath = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Factorio\\data\\base\\prototypes\\recipe";

            string currentFilename = String.Empty;
            List<RecipeGroup> recipeGroups = new List<RecipeGroup>();
            try
            {
                List<string> paths = new List<string>();
                foreach (string path in Directory.EnumerateFiles(folderPath, "*.lua"))
                {
                    if (FileStartsWith(path, "data:extend"))
                        paths.Add(path);
                }
                foreach (string path in paths)
                {
                    currentFilename = path;
                    string text = File.ReadAllText(path);
                    var parser = new LuaData.Parser(text);
                    LuaData.Value valueRoot = parser.Parse();

                    string baseName = Path.GetFileName(path);
                    List<RecipeGroup> recipes = ConvertLuaToRecipeList.Load(valueRoot);
                    recipeGroups.AddRange(recipes);
                }
            }
            catch (IOException ex)
            {
                Debug.WriteLine(currentFilename);
                Debug.WriteLine(ex.Message);
            }

            var recipeDictionary = new Dictionary<string, Recipe>();
            foreach (RecipeGroup rg in recipeGroups)
            {
                Recipe r = rg.Normal;
                recipeDictionary.Add(r.Name, r);
            }
            return new RecipeBook(recipeDictionary);
        }

        private static bool FileStartsWith(string name, string text)
        {
            foreach (string line in File.ReadLines(name))
            {
                int index = 0;
                while (index < line.Length && Char.IsWhiteSpace(line[index]))
                    index += 1;
                if (index == line.Length)
                    continue;

                foreach (Char ch in text)
                    if (ch != line[index++])
                        return false;
                return true;
            }
            return false;
        }
    }
}
