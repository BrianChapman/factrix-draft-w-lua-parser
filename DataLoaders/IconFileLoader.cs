﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    class IconFileLoader
    {
        public static IconBook LoadIcons(string folderPath = null)
        {
            string[] iconFiles = { "item.lua" };
            if (string.IsNullOrEmpty(folderPath))
            {
                folderPath = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Factorio\\data\\base\\prototypes\\item";
                
            }


            string currentFilename = String.Empty;
            List<IconDefinition> iconList = new List<IconDefinition>();
            try
            {
                List<string> paths = new List<string>();
                //foreach (string path in Directory.EnumerateFiles(folderPath, "*.lua"))
                foreach(string file in iconFiles)
                {
                    string path = Path.Combine(folderPath, file);
                    if (FileStartsWith(path, "data:extend"))
                        paths.Add(path);
                }
                foreach (string path in paths)
                {
                    currentFilename = path;
                    string text = File.ReadAllText(path);
                    var parser = new LuaData.Parser(text);
                    LuaData.Value valueRoot = parser.Parse();

                    string baseName = Path.GetFileName(path);
                    List<IconDefinition> icons = ConvertIconList.Load(valueRoot);
                    iconList.AddRange(icons);
                }
            }
            catch (IOException ex)
            {
                Debug.WriteLine(currentFilename);
                Debug.WriteLine(ex.Message);
            }

            return new IconBook(iconList);
        }

        private static bool FileStartsWith(string name, string text)
        {
            foreach (string line in File.ReadLines(name))
            {
                int index = 0;
                while (index < line.Length && Char.IsWhiteSpace(line[index]))
                    index += 1;
                if (index == line.Length)
                    continue;

                foreach (Char ch in text)
                    if (ch != line[index++])
                        return false;
                return true;
            }
            return false;
        }


    }
}

