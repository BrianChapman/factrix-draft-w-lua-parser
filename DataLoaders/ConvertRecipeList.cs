﻿using LuaData;
using System;
using System.Collections.Generic;

namespace Factrix
{
    static class ConvertLuaToRecipeList
    {
        public static List<RecipeGroup> Load(Value valueTree)
        {
            return CollectItems(valueTree.ValueList);
        }

        static List<RecipeGroup> CollectItems(List<Value> valueList)
        {
            List<RecipeGroup> recipeList = new List<RecipeGroup>();
            foreach (Value value in valueList)
            {
                if (value.Type == Values.PropertyBag)
                {
                    RecipeGroup recipGroup = MakeRecipeGroup(value.PropertyBag);
                    if (recipGroup != null)
                        recipeList.Add(recipGroup);
                }
            }
            return recipeList;
        }

        static RecipeGroup MakeRecipeGroup(PropertyBag bag)
        {
            Value value;
            if (!bag.TryGetValue("type", out value))
                return null;

            if (value.String != "recipe")
                throw new InvalidOperationException("Non-recipe found");

            string itemName = bag.GetString("name");
            string categoryString = bag.GetString("category", "crafting");
            float resultCount = bag.GetNumber("result_count", 1);
            CraftingCategory category = CraftingCategories.Parse(categoryString);

            Recipe normalRecipe = null;
            Recipe expensiveRecipe = null;
            if (bag.TryGetValue("normal", out value))
                normalRecipe = MakeRecipe(itemName, category, value.PropertyBag);
            if (bag.TryGetValue("expensive", out value))
                expensiveRecipe = MakeRecipe(itemName, category, value.PropertyBag);

            if (normalRecipe == null)
                normalRecipe = MakeRecipe(itemName, category, bag);

            return new RecipeGroup(normalRecipe, expensiveRecipe);
        }

        static Recipe MakeRecipe(string name, CraftingCategory category, PropertyBag bag)
        {
            float energy = bag.GetNumber("energy_required", 0.5f);
            ValueList ingredientList = bag.GetValueList("ingredients");
            List<ItemAmount> ingredients = ConvertListOrBagToItemList(ingredientList);

            ValueList resultList = GetResults(bag);
            List<ItemAmount> results = ConvertListOrBagToItemList(resultList);

            return new Recipe(name, category, energy, ingredients, results);
        }

        static ValueList GetResults(PropertyBag bag)
        {
            string result = bag.GetString("result", null);
            if (result == null)
                return bag.GetValueList("results");

            var innerList = new ValueList();
            innerList.Add(new Value(result, 0));

            float resultCount = bag.GetNumber("result_count", 1);
            innerList.Add(new Value(resultCount, 0));

            var outerList = new ValueList();
            outerList.Add(new Value(innerList, 0));
            return outerList;
        }

        static List<ItemAmount> ConvertListOrBagToItemList(ValueList valueList)
        {
            List<ItemAmount> list = new List<ItemAmount>();
            foreach (Value collection in valueList)
            {
                string itemName = null;
                float amount = -1;
                ItemType materialType = ItemType.Item;
                float probability = 1;

                if (collection.Type == Values.ValueList)
                {
                    foreach (Value v in collection.ValueList)
                    {
                        if (v.Type == Values.String)
                            itemName = v.String;
                        if (v.Type == Values.Number)
                            amount = v.Number;
                    }
                }
                else if (collection.Type == Values.PropertyBag)
                {
                    PropertyBag bag = collection.PropertyBag;
                    itemName = bag.GetString("name");
                    amount = bag.GetNumber("amount");
                    string typeString = bag.GetString("type", "item");
                    if (typeString == "fluid")
                        materialType = ItemType.Fluid;
                    probability = bag.GetNumber("probability", 1);
                }
                if (itemName == null || amount == -1)
                    throw new InvalidOperationException("Converting Ingredient List missing Name or Quantity");

                var ingredient = new ItemAmount(itemName, amount, materialType, probability);
                list.Add(ingredient);
            }
            return list;
        }
    }
}
