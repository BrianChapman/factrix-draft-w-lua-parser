﻿using LuaData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    class ConvertIconList
    {
        public static List<IconDefinition> Load(Value valueTree)
        {
            return CollectItems(valueTree.ValueList);
        }

        static List<IconDefinition> CollectItems(List<Value> valueList)
        {
            List<IconDefinition> recipeList = new List<IconDefinition>();
            foreach (Value value in valueList)
            {
                if (value.Type == Values.PropertyBag)
                {
                    IconDefinition recipGroup = MakeIconDefinition(value.PropertyBag);
                    if (recipGroup != null)
                        recipeList.Add(recipGroup);
                }
            }
            return recipeList;
        }

        static IconDefinition MakeIconDefinition(PropertyBag bag)
        {
            return new IconDefinition("xx", "yy");
        }

    }
}
