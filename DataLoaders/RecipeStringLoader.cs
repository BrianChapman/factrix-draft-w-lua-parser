﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    static class RecipeStringLoader
    {
        static Char[] semicolon = { ';' };
        static Char[] colon = { ':' };
        static Char[] space = { ' ' };
        static Char[] comma = { ',' };

        public static RecipeBook Load()
        {
            Dictionary<string, Recipe> rdict;
            try
            {
                rdict = LoadRecipeBook();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                Debug.WriteLine(string.Format("Error: {0}", msg));
                throw;
            }
            RecipeBook rb = new RecipeBook(rdict);
            return rb;
        }

        public static Dictionary<string, Recipe> LoadRecipeBook()
        {
            var recipeDictionary = new Dictionary<string, Recipe>();

            string[] lines = ItemDataProvider.source.Split(semicolon, StringSplitOptions.RemoveEmptyEntries);
            foreach (string rawLine in lines)
            {
                if (string.IsNullOrWhiteSpace(rawLine))
                    continue;
                string line = rawLine.Trim(' ', '\r', '\n');
                string[] sections = line.Split(colon);
                if (sections.Length != 4)
                    throw new InvalidOperationException("there must be 3 ':'s per line.");
                string[] header = sections[0].Split(space, StringSplitOptions.RemoveEmptyEntries);

                float energy = float.Parse(header[0].Trim());
                string recipeName = header[1].Trim();

                CraftingCategory category = CraftingCategory.Crafting;
                if (!string.IsNullOrEmpty(sections[1]))
                    category = (CraftingCategory)Enum.Parse(typeof(CraftingCategory), sections[1]);

                List<ItemAmount> Inputs = ParseAmountList(sections[2]);
                List<ItemAmount> Outputs = ParseAmountList(sections[3]); ;

                var r = new Recipe(recipeName, category, energy, Inputs, Outputs);
                recipeDictionary.Add(recipeName, r);
            }
            return recipeDictionary;
        }

        public static List<ItemAmount> ParseAmountList(string amountString)
        {
            List<ItemAmount> list = new List<ItemAmount>();
            string[] sets = amountString.Split(comma, StringSplitOptions.RemoveEmptyEntries);
            foreach (string set in sets)
            {
                string[] parts = set.Split(space, StringSplitOptions.RemoveEmptyEntries);
                int amount = int.Parse(parts[0]);
                string name = parts[1].Trim();
                ItemType type = ItemType.Item;
                if (parts.Length > 2)
                    type = (ItemType)Enum.Parse(typeof(ItemType), parts[2]);
                float probability = 1;
                if (parts.Length > 3)
                    probability = float.Parse(parts[3]);
                list.Add(new ItemAmount(name, amount, type, probability));
            }
            return list;
        }
    }

    //    class ItemDataProvider
    //    {
    //        public const string source = @"
    // 0   iron-ore::1 iron-ore;
    // 0   copper-ore::1 copper-ore;
    // 0   plastic-bar::1 plastic-bar;
    // 3.5 iron-plate: 1 iron-ore: 1 iron-plate; 
    // 0.5 gear: 2 iron-plate: 1 gear;
    // 3.5 copper-plate: 1 copper-ore: 1 copper-plate;
    // 0.5 copper-cable: 1 copper-plate: 2 copper-cable;
    // 0.5 green-circuit: 3 copper-cable, 1 iron-plate: 1 green-circuit;
    // 6.0 red-circuit: 2 green-circuit, 4 copper-cable, 2 plastic-bar: 1 red-circuit;
    // 0.5 inserter: 1 green-circuit, 1 gear, 1 iron-plate: 1 inserter;
    // 0.5 transport-belt: 1 gear, 1 iron-plate: 2 transport-belt;
    // 5.0 red-pack: 1 copper-plate, 1 gear: 1 red-pack;
    // 6.0 green-pack: 1 inserter, 1 transport-belt: 1 green-pack;
    //12.0 blue-pack: 1 electric-miner, 1 red-circuit, 1 engine-unit: 1 blue-pack;
    //10.0 engine-unit: 1 steel-plate, 1 gear, 2 pipe: 1 engine-unit;
    // 2.0 electric-miner: 3 green-circuit, 5 gear, 10 iron-plate: 1 electric-miner;
    //17.5 steel-plate: 5 iron-plate: 1 steel-plate;
    // 0.5 pipe: 2 iron-plate: 1 pipe;";
    //    }

    class ItemDataProvider
    {
        public const string source = @"
3 piercing-rounds-magazine::1 firearm-magazine, 1 steel-plate, 5 copper-plate:1 piercing-rounds-magazine;
10 uranium-rounds-magazine::1 piercing-rounds-magazine, 1 uranium-238:1 uranium-rounds-magazine;
8 rocket::1 electronic-circuit, 1 explosives, 2 iron-plate:1 rocket;
8 explosive-rocket::1 rocket, 2 explosives:1 explosive-rocket;
50 atomic-bomb::20 processing-unit, 10 explosives, 30 uranium-235:1 atomic-bomb;
3 shotgun-shell::2 copper-plate, 2 iron-plate:1 shotgun-shell;
8 piercing-shotgun-shell::2 shotgun-shell, 5 copper-plate, 2 steel-plate:1 piercing-shotgun-shell;
8 railgun-dart::5 steel-plate, 5 electronic-circuit:1 railgun-dart;
8 cannon-shell::2 steel-plate, 2 plastic-bar, 1 explosives:1 cannon-shell;
8 explosive-cannon-shell::2 steel-plate, 2 plastic-bar, 2 explosives:1 explosive-cannon-shell;
12 uranium-cannon-shell::1 cannon-shell, 1 uranium-238:1 uranium-cannon-shell;
12 explosive-uranium-cannon-shell::1 explosive-cannon-shell, 1 uranium-238:1 explosive-uranium-cannon-shell;
6 flamethrower-ammo:Chemistry:5 steel-plate, 50 light-oil Fluid, 50 heavy-oil Fluid:1 flamethrower-ammo;
8 poison-capsule::3 steel-plate, 3 electronic-circuit, 10 coal:1 poison-capsule;
8 slowdown-capsule::2 steel-plate, 2 electronic-circuit, 5 coal:1 slowdown-capsule;
8 grenade::5 iron-plate, 10 coal:1 grenade;
8 cluster-grenade::7 grenade, 5 explosives, 5 steel-plate:1 cluster-grenade;
8 defender-capsule::1 piercing-rounds-magazine, 2 electronic-circuit, 3 iron-gear-wheel:1 defender-capsule;
15 distractor-capsule::4 defender-capsule, 3 advanced-circuit:1 distractor-capsule;
15 destroyer-capsule::4 distractor-capsule, 1 speed-module:1 destroyer-capsule;
0.5 discharge-defense-remote::1 electronic-circuit:1 discharge-defense-remote;
3.5 copper-plate:Smelting:1 copper-ore:1 copper-plate;
3.5 iron-plate:Smelting:1 iron-ore:1 iron-plate;
3.5 stone-brick:Smelting:2 stone:1 stone-brick;
0.5 wood::1 raw-wood:1 wood;
0.5 wooden-chest::4 wood:1 wooden-chest;
0.5 iron-stick::1 iron-plate:1 iron-stick;
0.5 iron-axe::2 iron-stick, 3 iron-plate:1 iron-axe;
0.5 stone-furnace::5 stone:1 stone-furnace;
0.5 boiler::1 stone-furnace, 4 pipe:1 boiler;
0.5 steam-engine::8 iron-gear-wheel, 5 pipe, 10 iron-plate:1 steam-engine;
0.5 iron-gear-wheel::2 iron-plate:1 iron-gear-wheel;
0.5 electronic-circuit::1 iron-plate, 3 copper-cable:1 electronic-circuit;
0.5 transport-belt::1 iron-plate, 1 iron-gear-wheel:1 transport-belt;
2 electric-mining-drill::3 electronic-circuit, 5 iron-gear-wheel, 10 iron-plate:1 electric-mining-drill;
2 burner-mining-drill::3 iron-gear-wheel, 1 stone-furnace, 3 iron-plate:1 burner-mining-drill;
0.5 inserter::1 electronic-circuit, 1 iron-gear-wheel, 1 iron-plate:1 inserter;
0.5 burner-inserter::1 iron-plate, 1 iron-gear-wheel:1 burner-inserter;
0.5 pipe::1 iron-plate:1 pipe;
0.5 offshore-pump::2 electronic-circuit, 1 pipe, 1 iron-gear-wheel:1 offshore-pump;
0.5 copper-cable::1 copper-plate:1 copper-cable;
0.5 small-electric-pole::2 wood, 2 copper-cable:1 small-electric-pole;
5 pistol::5 copper-plate, 5 iron-plate:1 pistol;
10 submachine-gun::10 iron-gear-wheel, 5 copper-plate, 10 iron-plate:1 submachine-gun;
1 firearm-magazine::4 iron-plate:1 firearm-magazine;
3 light-armor::40 iron-plate:1 light-armor;
0.5 radar::5 electronic-circuit, 5 iron-gear-wheel, 10 iron-plate:1 radar;
0.5 small-lamp::1 electronic-circuit, 3 iron-stick, 1 iron-plate:1 small-lamp;
0.5 pipe-to-ground::10 pipe, 5 iron-plate:1 pipe-to-ground;
0.5 assembling-machine-1::3 electronic-circuit, 5 iron-gear-wheel, 9 iron-plate:1 assembling-machine-1;
0.5 repair-pack::2 electronic-circuit, 2 iron-gear-wheel:1 repair-pack;
8 gun-turret::10 iron-gear-wheel, 10 copper-plate, 20 iron-plate:1 gun-turret;
10 night-vision-equipment::5 advanced-circuit, 10 steel-plate:1 night-vision-equipment;
10 energy-shield-equipment::5 advanced-circuit, 10 steel-plate:1 energy-shield-equipment;
10 energy-shield-mk2-equipment::10 energy-shield-equipment, 10 processing-unit:1 energy-shield-mk2-equipment;
10 battery-equipment::5 battery, 10 steel-plate:1 battery-equipment;
10 battery-mk2-equipment::10 battery-equipment, 20 processing-unit:1 battery-mk2-equipment;
10 solar-panel-equipment::5 solar-panel, 1 advanced-circuit, 5 steel-plate:1 solar-panel-equipment;
10 fusion-reactor-equipment::250 processing-unit:1 fusion-reactor-equipment;
10 personal-laser-defense-equipment::1 processing-unit, 5 steel-plate, 5 laser-turret:1 personal-laser-defense-equipment;
10 discharge-defense-equipment::5 processing-unit, 20 steel-plate, 10 laser-turret:1 discharge-defense-equipment;
10 exoskeleton-equipment::10 processing-unit, 30 electric-engine-unit, 20 steel-plate:1 exoskeleton-equipment;
10 personal-roboport-equipment::10 advanced-circuit, 40 iron-gear-wheel, 20 steel-plate, 45 battery:1 personal-roboport-equipment;
20 personal-roboport-mk2-equipment::5 personal-roboport-equipment, 100 processing-unit:1 personal-roboport-mk2-equipment;
5 basic-oil-processing:OilProcessing:100 crude-oil Fluid:30 heavy-oil Fluid, 30 light-oil Fluid, 40 petroleum-gas Fluid;
5 advanced-oil-processing:OilProcessing:50 water Fluid, 100 crude-oil Fluid:10 heavy-oil Fluid, 45 light-oil Fluid, 55 petroleum-gas Fluid;
5 coal-liquefaction:OilProcessing:10 coal, 25 heavy-oil Fluid, 50 steam Fluid:35 heavy-oil Fluid, 15 light-oil Fluid, 20 petroleum-gas Fluid;
3 heavy-oil-cracking:Chemistry:30 water Fluid, 40 heavy-oil Fluid:30 light-oil Fluid;
3 light-oil-cracking:Chemistry:30 water Fluid, 30 light-oil Fluid:20 petroleum-gas Fluid;
1 sulfuric-acid:Chemistry:5 sulfur, 1 iron-plate, 100 water Fluid:50 sulfuric-acid Fluid;
1 plastic-bar:Chemistry:20 petroleum-gas Fluid, 1 coal:2 plastic-bar;
3 solid-fuel-from-light-oil:Chemistry:10 light-oil Fluid:1 solid-fuel;
3 solid-fuel-from-petroleum-gas:Chemistry:20 petroleum-gas Fluid:1 solid-fuel;
3 solid-fuel-from-heavy-oil:Chemistry:20 heavy-oil Fluid:1 solid-fuel;
1 sulfur:Chemistry:30 water Fluid, 30 petroleum-gas Fluid:2 sulfur;
1 lubricant:Chemistry:10 heavy-oil Fluid:10 lubricant Fluid;
1 empty-barrel:Crafting:1 steel-plate:1 empty-barrel;
17.5 steel-plate:Smelting:5 iron-plate:1 steel-plate;
0.5 long-handed-inserter::1 iron-gear-wheel, 1 iron-plate, 1 inserter:1 long-handed-inserter;
0.5 fast-inserter::2 electronic-circuit, 2 iron-plate, 1 inserter:1 fast-inserter;
0.5 filter-inserter::1 fast-inserter, 4 electronic-circuit:1 filter-inserter;
0.5 stack-inserter::15 iron-gear-wheel, 15 electronic-circuit, 1 advanced-circuit, 1 fast-inserter:1 stack-inserter;
0.5 stack-filter-inserter::1 stack-inserter, 5 electronic-circuit:1 stack-filter-inserter;
15 speed-module::5 advanced-circuit, 5 electronic-circuit:1 speed-module;
30 speed-module-2::4 speed-module, 5 advanced-circuit, 5 processing-unit:1 speed-module-2;
60 speed-module-3::5 speed-module-2, 5 advanced-circuit, 5 processing-unit:1 speed-module-3;
15 productivity-module::5 advanced-circuit, 5 electronic-circuit:1 productivity-module;
30 productivity-module-2::4 productivity-module, 5 advanced-circuit, 5 processing-unit:1 productivity-module-2;
60 productivity-module-3::5 productivity-module-2, 5 advanced-circuit, 5 processing-unit:1 productivity-module-3;
15 effectivity-module::5 advanced-circuit, 5 electronic-circuit:1 effectivity-module;
30 effectivity-module-2::4 effectivity-module, 5 advanced-circuit, 5 processing-unit:1 effectivity-module-2;
60 effectivity-module-3::5 effectivity-module-2, 5 advanced-circuit, 5 processing-unit:1 effectivity-module-3;
0.5 player-port::10 electronic-circuit, 5 iron-gear-wheel, 1 iron-plate:1 player-port;
0.5 fast-transport-belt::5 iron-gear-wheel, 1 transport-belt:1 fast-transport-belt;
0.5 express-transport-belt:CraftingWithFluid:10 iron-gear-wheel, 1 fast-transport-belt, 20 lubricant Fluid:1 express-transport-belt;
10 solar-panel::5 steel-plate, 15 electronic-circuit, 5 copper-plate:1 solar-panel;
0.5 assembling-machine-2::9 iron-plate, 3 electronic-circuit, 5 iron-gear-wheel, 1 assembling-machine-1:1 assembling-machine-2;
0.5 assembling-machine-3::4 speed-module, 2 assembling-machine-2:1 assembling-machine-3;
0.5 car::8 engine-unit, 20 iron-plate, 5 steel-plate:1 car;
0.5 tank::32 engine-unit, 50 steel-plate, 15 iron-gear-wheel, 10 advanced-circuit:1 tank;
0.5 rail::1 stone, 1 iron-stick, 1 steel-plate:1 rail;
0.5 locomotive::20 engine-unit, 10 electronic-circuit, 30 steel-plate:1 locomotive;
0.5 cargo-wagon::10 iron-gear-wheel, 20 iron-plate, 20 steel-plate:1 cargo-wagon;
1.5 fluid-wagon::10 iron-gear-wheel, 16 steel-plate, 8 pipe, 3 storage-tank:1 fluid-wagon;
0.5 train-stop::5 electronic-circuit, 10 iron-plate, 3 steel-plate:1 train-stop;
0.5 rail-signal::1 electronic-circuit, 5 iron-plate:1 rail-signal;
0.5 rail-chain-signal::1 electronic-circuit, 5 iron-plate:1 rail-chain-signal;
8 heavy-armor::100 copper-plate, 50 steel-plate:1 heavy-armor;
15 modular-armor::30 advanced-circuit, 50 steel-plate:1 modular-armor;
20 power-armor::40 processing-unit, 20 electric-engine-unit, 40 steel-plate:1 power-armor;
25 power-armor-mk2::5 effectivity-module-3, 5 speed-module-3, 40 processing-unit, 40 steel-plate:1 power-armor-mk2;
0.5 iron-chest::8 iron-plate:1 iron-chest;
0.5 steel-chest::8 steel-plate:1 steel-chest;
0.5 stone-wall::5 stone-brick:1 stone-wall;
0.5 gate::1 stone-wall, 2 steel-plate, 2 electronic-circuit:1 gate;
10 flamethrower::5 steel-plate, 10 iron-gear-wheel:1 flamethrower;
5 land-mine::1 steel-plate, 2 explosives:1 land-mine;
10 rocket-launcher::5 iron-plate, 5 iron-gear-wheel, 5 electronic-circuit:1 rocket-launcher;
10 shotgun::15 iron-plate, 5 iron-gear-wheel, 10 copper-plate, 5 wood:1 shotgun;
10 combat-shotgun::15 steel-plate, 5 iron-gear-wheel, 10 copper-plate, 10 wood:1 combat-shotgun;
8 railgun::15 steel-plate, 15 copper-plate, 10 electronic-circuit, 5 advanced-circuit:1 railgun;
5 science-pack-1::1 copper-plate, 1 iron-gear-wheel:1 science-pack-1;
6 science-pack-2::1 inserter, 1 transport-belt:1 science-pack-2;
12 science-pack-3::1 advanced-circuit, 1 engine-unit, 1 electric-mining-drill:1 science-pack-3;
10 military-science-pack::1 piercing-rounds-magazine, 1 grenade, 1 gun-turret:1 military-science-pack;
14 production-science-pack::1 electric-engine-unit, 1 assembling-machine-1, 1 electric-furnace:1 production-science-pack;
14 high-tech-science-pack::1 battery, 3 processing-unit, 1 speed-module, 30 copper-cable:1 high-tech-science-pack;
3 lab::10 electronic-circuit, 10 iron-gear-wheel, 4 transport-belt:1 lab;
0.5 red-wire::1 electronic-circuit, 1 copper-cable:1 red-wire;
0.5 green-wire::1 electronic-circuit, 1 copper-cable:1 green-wire;
1 underground-belt::10 iron-plate, 5 transport-belt:1 underground-belt;
0.5 fast-underground-belt::40 iron-gear-wheel, 2 underground-belt:1 fast-underground-belt;
0.5 express-underground-belt:CraftingWithFluid:80 iron-gear-wheel, 2 fast-underground-belt, 40 lubricant Fluid:1 express-underground-belt;
1 loader::5 inserter, 5 electronic-circuit, 5 iron-gear-wheel, 5 iron-plate, 5 transport-belt:1 loader;
3 fast-loader::5 fast-transport-belt, 1 loader:1 fast-loader;
10 express-loader::5 express-transport-belt, 1 fast-loader:1 express-loader;
1 splitter::5 electronic-circuit, 5 iron-plate, 4 transport-belt:1 splitter;
2 fast-splitter::1 splitter, 10 iron-gear-wheel, 10 electronic-circuit:1 fast-splitter;
2 express-splitter:CraftingWithFluid:1 fast-splitter, 10 iron-gear-wheel, 10 advanced-circuit, 80 lubricant Fluid:1 express-splitter;
6 advanced-circuit::2 electronic-circuit, 2 plastic-bar, 4 copper-cable:1 advanced-circuit;
10 processing-unit:CraftingWithFluid:20 electronic-circuit, 2 advanced-circuit, 5 sulfuric-acid Fluid:1 processing-unit;
0.5 logistic-robot::1 flying-robot-frame, 2 advanced-circuit:1 logistic-robot;
0.5 construction-robot::1 flying-robot-frame, 2 electronic-circuit:1 construction-robot;
0.5 logistic-chest-passive-provider::1 steel-chest, 3 electronic-circuit, 1 advanced-circuit:1 logistic-chest-passive-provider;
0.5 logistic-chest-active-provider::1 steel-chest, 3 electronic-circuit, 1 advanced-circuit:1 logistic-chest-active-provider;
0.5 logistic-chest-storage::1 steel-chest, 3 electronic-circuit, 1 advanced-circuit:1 logistic-chest-storage;
0.5 logistic-chest-requester::1 steel-chest, 3 electronic-circuit, 1 advanced-circuit:1 logistic-chest-requester;
30 rocket-silo::1000 steel-plate, 1000 concrete, 100 pipe, 200 processing-unit, 200 electric-engine-unit:1 rocket-silo;
10 roboport::45 steel-plate, 45 iron-gear-wheel, 45 advanced-circuit:1 roboport;
0.5 steel-axe::5 steel-plate, 2 iron-stick:1 steel-axe;
0.5 big-electric-pole::5 steel-plate, 5 copper-plate:1 big-electric-pole;
0.5 substation::10 steel-plate, 5 advanced-circuit, 5 copper-plate:1 substation;
0.5 medium-electric-pole::2 steel-plate, 2 copper-plate:1 medium-electric-pole;
10 accumulator::2 iron-plate, 5 battery:1 accumulator;
3 steel-furnace::6 steel-plate, 10 stone-brick:1 steel-furnace;
5 electric-furnace::10 steel-plate, 5 advanced-circuit, 10 stone-brick:1 electric-furnace;
15 beacon::20 electronic-circuit, 20 advanced-circuit, 10 steel-plate, 10 copper-cable:1 beacon;
5 pumpjack::5 steel-plate, 10 iron-gear-wheel, 5 electronic-circuit, 10 pipe:1 pumpjack;
10 oil-refinery::15 steel-plate, 10 iron-gear-wheel, 10 stone-brick, 10 electronic-circuit, 10 pipe:1 oil-refinery;
10 engine-unit:AdvancedCrafting:1 steel-plate, 1 iron-gear-wheel, 2 pipe:1 engine-unit;
10 electric-engine-unit:CraftingWithFluid:1 engine-unit, 15 lubricant Fluid, 2 electronic-circuit:1 electric-engine-unit;
20 flying-robot-frame::1 electric-engine-unit, 2 battery, 1 steel-plate, 3 electronic-circuit:1 flying-robot-frame;
5 explosives:Chemistry:1 sulfur, 1 coal, 10 water Fluid:1 explosives;
5 battery:Chemistry:20 sulfuric-acid Fluid, 1 iron-plate, 1 copper-plate:1 battery;
3 storage-tank::20 iron-plate, 5 steel-plate:1 storage-tank;
2 pump::1 engine-unit, 1 steel-plate, 1 pipe:1 pump;
5 chemical-plant::5 steel-plate, 5 iron-gear-wheel, 5 electronic-circuit, 5 pipe:1 chemical-plant;
30 small-plane:Crafting:100 plastic-bar, 200 advanced-circuit, 20 electric-engine-unit, 100 battery:1 small-plane;
0.5 arithmetic-combinator::5 copper-cable, 5 electronic-circuit:1 arithmetic-combinator;
0.5 decider-combinator::5 copper-cable, 5 electronic-circuit:1 decider-combinator;
0.5 constant-combinator::5 copper-cable, 2 electronic-circuit:1 constant-combinator;
2 power-switch::5 iron-plate, 5 copper-cable, 2 electronic-circuit:1 power-switch;
2 programmable-speaker::5 iron-plate, 5 copper-cable, 4 electronic-circuit:1 programmable-speaker;
30 low-density-structure:Crafting:10 steel-plate, 5 copper-plate, 5 plastic-bar:1 low-density-structure;
30 rocket-fuel:Crafting:10 solid-fuel:1 rocket-fuel;
30 rocket-control-unit:Crafting:1 processing-unit, 1 speed-module:1 rocket-control-unit;
3 rocket-part:RocketBuilding:10 low-density-structure, 10 rocket-fuel, 10 rocket-control-unit:1 rocket-part;
3 satellite:Crafting:100 low-density-structure, 100 solar-panel, 100 accumulator, 5 radar, 100 processing-unit, 50 rocket-fuel:1 satellite;
10 concrete:CraftingWithFluid:5 stone-brick, 1 iron-ore, 100 water Fluid:1 concrete;
0.25 hazard-concrete:Crafting:10 concrete:1 hazard-concrete;
0.5 landfill:Crafting:20 stone:1 landfill;
0.5 electric-energy-interface::2 iron-plate, 5 electronic-circuit:1 electric-energy-interface;
4 nuclear-reactor::500 concrete, 500 steel-plate, 500 advanced-circuit, 500 copper-plate:1 nuclear-reactor;
4 centrifuge::100 concrete, 50 steel-plate, 100 advanced-circuit, 100 iron-gear-wheel:1 centrifuge;
10 uranium-processing:Centrifuging:10 uranium-ore:1 uranium-235 Item 0.007, 1 uranium-238 Item 0.993;
50 kovarex-enrichment-process:Centrifuging:40 uranium-235, 5 uranium-238:41 uranium-235, 2 uranium-238;
50 nuclear-fuel-reprocessing:Centrifuging:5 used-up-uranium-fuel-cell:3 uranium-238;
10 uranium-fuel-cell::10 iron-plate, 1 uranium-235, 19 uranium-238:1 uranium-fuel-cell;
0.5 heat-exchanger::10 steel-plate, 100 copper-plate, 10 pipe:1 heat-exchanger;
0.5 heat-pipe::10 steel-plate, 20 copper-plate:1 heat-pipe;
0.5 steam-turbine::50 iron-gear-wheel, 50 copper-plate, 20 pipe:1 steam-turbine;
20 laser-turret::20 steel-plate, 20 electronic-circuit, 12 battery:1 laser-turret;
20 flamethrower-turret::30 steel-plate, 15 iron-gear-wheel, 10 pipe, 5 engine-unit:1 flamethrower-turret;
";
    }

}
