﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{

    class MainDataContext : INotifyPropertyChanged
    {
        public MainDataContext()
        {
            var list = new ObservableCollection<ItemRow>();
            list.Add(new ItemRow("Science-Pack-2"));
            list.Add(new ItemRow("Inserter"));
            list.Add(new ItemRow("electronic-circuit"));

            ProductionList = list;
        }

        private IconBook _iconBook;
        public IconBook IconBook
        {
            get
            {
                if (_iconBook == null)
                {
                    _iconBook = IconFileLoader.LoadIcons();
                }
                return _iconBook;
            }
        }

        public ObservableCollection<ItemRow> ProductionList { get; set; }

        private ItemRow _selectedItemRow;
        public ItemRow SelectedItemRow
        {
            get { return _selectedItemRow; }
            set
            {
                _selectedItemRow = value;
                NotifyChange("SelectedItemRow");
                NotifyChange("IsARowSelected");
            }
        }
        public bool IsARowSelected { get { return SelectedItemRow != null; } }


        private bool _showMatrix = false;

        public bool ShowMatrix
        {
            get { return _showMatrix; }
            set
            {
                _showMatrix = value;
                NotifyChange("ShowMatrix");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyChange(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

    }

}