﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    enum Crafter { Belt, Pipe, Assembler1, Assembler2, ChemicalPlant }

    class ItemMethodRow : INotifyPropertyChanged
    {
        static List<int> s_count0to3;

        static ItemMethodRow()
        {
            s_count0to3 = new List<int>() { 0, 1, 2, 3 };

        }
        public List<String> Recipes { get; set; }
        public String Recipe { get; set; }
        public float RecipeTime { get; set; }
        public int Count { get; set; }
        public Crafter DeviceType { get; set; }
        public List<Crafter> DeviceTypes { get; set; }
        public float SpeedFactor { get; set; }

        public int NumberOfSpeedModules { get; set; }
        public List<int> SpeedModulesChoice { get; }

        public int NumberOfProductivityModules { get; set; }
        public List<int> ProductivityModulesChoice { get; }

        public ItemMethodRow(ItemRow parent, Crafter deviceType)
        {
            Count = 1;
            Recipes = new List<string>();
            Recipes.Add(parent.Item);
            Recipe = parent.Item;

            RecipeTime = 2.0f;

            DeviceType = deviceType;
            DeviceTypes = new List<Crafter>();
            DeviceTypes.Add(Crafter.Belt);
            DeviceTypes.Add(Crafter.Assembler1);
            DeviceTypes.Add(Crafter.Assembler2);

            SpeedFactor = 0.75f;

            SpeedModulesChoice = s_count0to3;
            NumberOfSpeedModules = 0;

            ProductivityModulesChoice = s_count0to3;
            NumberOfSpeedModules = 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyChange(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

}
