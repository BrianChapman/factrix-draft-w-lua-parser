﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix
{
    enum MakeMethod { Delivery, Build }

    class ItemRow : INotifyPropertyChanged
    {
        static private List<MakeMethod> s_makeMethodList;

        static ItemRow()
        {
            s_makeMethodList = new List<MakeMethod>();
            s_makeMethodList.Add(MakeMethod.Delivery);
            s_makeMethodList.Add(MakeMethod.Build);
        }
        public String Item { get; set; }

        public List<MakeMethod> MethodModes { get; set; }
        public MakeMethod MethodMode { get; set; }

        public ObservableCollection<ItemMethodRow> Devices { get; set; }
        public float TotalMade { get; set; }
        public float OverProduction { get; set; }
        public float[] MatrixRow { get; set; }
        public bool DontBalance { get; set; }

        public int NumberOfDevices
        {
            get
            {
                int count = 0;
                foreach (var methodRow in Devices)
                    count += methodRow.Count;
                return count;
            }
        }

        public ItemRow(string item)
        {
            Random r = new Random();
            Item = item;

            MethodModes = s_makeMethodList;
            MethodMode = MakeMethod.Delivery;

            Devices = new ObservableCollection<ItemMethodRow>();
            Devices.CollectionChanged += MethodRowsChanged;

            do
            {
                Devices.Add(new ItemMethodRow(this, Crafter.Assembler1));
            } while (r.Next() % 2 != 0);

            DontBalance = (r.Next() % 2) == 1;

            TotalMade = 6.0f;
            OverProduction = 1.5f;
            MatrixRow = new float[] { 2.6f, -1.7f, -0.02f };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyChange(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        private void MethodRowsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotifyChange("NumberOfDevices");
        }

    }


}
