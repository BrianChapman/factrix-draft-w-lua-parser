﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;

namespace WpfEx
{
    public class GridEx
    {
        public static readonly DependencyProperty GridColumnsProperty =
            DependencyProperty.RegisterAttached("GridColumns", typeof(String),
            typeof(GridEx),
            new PropertyMetadata(String.Empty, OnGridColumnsChanged));

        public static readonly DependencyProperty GridRowsProperty =
            DependencyProperty.RegisterAttached("GridRows", typeof(String),
            typeof(GridEx),
            new PropertyMetadata(String.Empty, OnGridRowsChanged));

        public static String GetGridColumns(DependencyObject obj)
        {
            return (String)obj.GetValue(GridColumnsProperty);
        }

        public static void SetGridColumns(DependencyObject obj, String value)
        {
            obj.SetValue(GridColumnsProperty, value);
        }

        public static String GetGridRows(Grid grid)
        {
            return (String)grid.GetValue(GridRowsProperty);
        }

        public static void SetGridRows(Grid grid, String value)
        {
            grid.SetValue(GridRowsProperty, value);
        }


        private static void OnGridRowsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Grid grid = d as Grid;
            if (grid == null)
                throw new InvalidOperationException("The attached property 'GridRows' can only be attached to a 'Grid' object");
            DefineGridRows(grid);
        }

        private static void OnGridColumnsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Grid grid = d as Grid;
            if (grid == null)
                throw new InvalidOperationException("The attached property 'GridColumns' can only be attached to a 'Grid' object");
            DefineGridColumns(grid);
        }

        private static List<GridLength> LengthStringConvert(String lengthsInput)
        {
            String[] lengths = lengthsInput.Split(',');
            List<GridLength> ret = new List<GridLength>();
            foreach (String length in lengths)
            {
                GridLength gl;
                switch (length.Trim().ToLower())
                {
                case "auto":
                    gl = new GridLength(1, GridUnitType.Auto);
                    break;
                case "*":
                case "":
                    gl = new GridLength(1, GridUnitType.Star);
                    break;
                default:
                    if (length.EndsWith("*"))
                    {
                        String numString = length.Substring(0, length.Length - 1);
                        double count;
                        if (Double.TryParse(numString, out count))
                        {
                            gl = new GridLength(count, GridUnitType.Star);
                            break;
                        }
                    }
                    else
                    {
                        double pixels;
                        if (double.TryParse(length, out pixels))
                        {
                            gl = new GridLength(Convert.ToDouble(pixels), GridUnitType.Pixel);
                            break;
                        }
                    }
                    throw new Exception("The value of the attached property 'GridRows' or'GridColumns" +
                      "must be a comma separated list of: " +
                      "'Auto', '*', 'n', or 'n*' (where n is a decimal number)");
                }
                ret.Add(gl);
            }
            return ret;
        }

        private static void DefineGridRows(Grid grid)
        {
            grid.RowDefinitions.Clear();
            String rowList = GetGridRows(grid);
            List<GridLength> LengthList = LengthStringConvert(rowList);
            foreach (GridLength length in LengthList)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = length });
            }
        }

        private static void DefineGridColumns(Grid grid)
        {
            grid.ColumnDefinitions.Clear();
            String columnList = GetGridColumns(grid);
            List<GridLength> LengthList = LengthStringConvert(columnList);
            foreach (GridLength length in LengthList)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = length });
            }
        }
    }
}
