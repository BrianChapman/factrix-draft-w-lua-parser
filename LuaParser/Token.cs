﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.LuaParser
{
    enum TokenType
    {
        // Content Tokens
        Name, Int, Float, Exp, Hex, NormalString, CharString, LongString,
        Comment, LineComment, Whitespace, NewLine,

        // Simple Tokens
        OpenParen, CloseParen, OpenBrace, CloseBrace, OpenSquareBracket, CloseSquareBracket,
        Colon, SemiColon, Comma, SingleEquals, Hash, ColonColon,
        Dot, DotDot, DotDotDot,
        Add, Subtract,
        Multiply, Divide, Modulo, FloorDivide,
        BitAnd, BitOr, BitNot, BitLeft, BitRight,
        Power, 
        Greater, Less, GreaterOrEqual, LessOrEqual, EqualEqual, NotEqual,

        // key words
        Nil, True, False, And, Or, Not,
        Do, While, Repeat, Until, If, Then, Else, Elseif, For, In, Function, Local, End, Return, Break, Goto,

        NumberOfTokens
    };

    class Token
    {
        public TokenType Type { get; private set; }
        public string Content { get; private set; }
        public int Line { get; private set; }
        public int Column { get; private set; }

        public Token(Tokenizer lineInfo, TokenType type, string content= null)
        {
            Type = type;
            Content = content;
            Line = lineInfo.LineNumber;
            Column = lineInfo.ColumnNumber;
        }
    }
}
