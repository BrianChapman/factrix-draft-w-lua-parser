﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.LuaParser
{
    enum NodeType {  Block, Statement, Empty, Label, Return, Break, Goto, Do, While, Repeat, If,
                    For, ForEach, 
                    NameAndArgs, Args, VariableSuffix, PrefixExpression,
                    FunctionCall, Variable, Expression, ParameterList, Field,
                    Function, FunctionDef, FunctionName, FunctionBody, LocalFunction,
                    LocalVariableDeclaration,
                    Assignment, StringArg, ExpressionList, FieldList, TableConstructor,
                    Number, String
    }

    enum NodeSubtype {  Name, IndexExpression, Value, ExpressionList, TableConstructor, String,
                        Indexed, DotNamed, Variable, Expression, Operator, SimpleValue
    }

    class Node
    {
        public NodeType Type { get; private set; }
        public Node(NodeType type) { Type = type; }
    }

    class BlockNode : Node
    {
        public List<StatementNode> Statements { get; private set; }
        public BlockNode(List<StatementNode> statements)
            : base(NodeType.Block)
        {
            Statements = statements;
        }
    }

    class StatementNode : Node
    {
        public StatementNode(NodeType type) : base(type) { }
    }

    class LabelStatementNode : StatementNode
    {
        public Token Name { get; private set; }
        public LabelStatementNode(Token name) : base(NodeType.Label) { Name = name; }
    }

    class ReturnStatementNode : StatementNode
    {
        public ExpressionNode Expression { get; private set; }
        public ReturnStatementNode(ExpressionNode expression) : base(NodeType.Return) { Expression = expression; }
    }

    class GotoStatementNode : StatementNode
    {
        public Token Name { get; private set; }
        public GotoStatementNode(Token name) : base(NodeType.Goto) { Name = name; }
    }

    class DoStatementNode : StatementNode
    {
        public BlockNode Block { get; private set; }
        public DoStatementNode(BlockNode block)
            : base(NodeType.Do)
        {
            Block = block;
        }
    }

    class WhileStatementNode : StatementNode
    {
        public ExpressionNode Expression { get; private set; }
        public BlockNode Block { get; private set; }
        public WhileStatementNode(ExpressionNode exp, BlockNode block)
            : base(NodeType.While)
        {
            Expression = exp;
            Block = block;
        }
    }

    class RepeatStatementNode : StatementNode
    {
        public ExpressionNode Expression { get; private set; }
        public BlockNode Block { get; private set; }
        public RepeatStatementNode(ExpressionNode exp, BlockNode block)
            : base(NodeType.Repeat)
        {
            Expression = exp;
            Block = block;
        }
    }

    class ElseIfNode
    {
        public ExpressionNode Expression { get; private set; }
        public BlockNode Then { get; private set; }

        public ElseIfNode(ExpressionNode exp, BlockNode thenBlk)
        {
            Expression = exp;
            Then = thenBlk;
        }
    }

    class NameAndArgsNode: Node
    {
        public Token ColonName { get; private set; }
        public ArgsNode Args { get; private set; }
        public NameAndArgsNode(Token colonName, ArgsNode args)
            : base(NodeType.NameAndArgs)
        {
            ColonName = colonName;
            Args = args;
        }
    }

    class ArgsNode : Node
    {
        public NodeSubtype Subtype { get; private set; }
        Node _args;
        public ExpressionListNode ExpressionList
        {
            get { return (ExpressionListNode)_args; }
        }
        public TableConstructorNode TableContructor
        {
            get { return (TableConstructorNode)_args; }
        }
        public StringNode String
        {
            get { return (StringNode)_args; }
        }
        public ArgsNode(Node args)
            :base(NodeType.Args)
        {

            _args = args;
            switch (args.Type)
            {
            case NodeType.ExpressionList:
                Subtype = NodeSubtype.ExpressionList;
                break;
            case NodeType.TableConstructor:
                Subtype = NodeSubtype.TableConstructor;
                break;
            case NodeType.String:
                Subtype = NodeSubtype.String;
                break;
            default:
                Debug.Assert(null == "Bad case in NameAndArgsNode Ctor");
                break;
            }
        }

    }

    class FieldNode : Node
    {
        public NodeSubtype Subtype { get; private set; }
        public Token Name { get; private set; }
        public ExpressionNode IndexExpression { get; private set; }
        public ExpressionNode Value { get; private set; }

        public FieldNode(Token name, ExpressionNode value)
            : base(NodeType.Field)
        {
            Subtype = NodeSubtype.Name;
            Name = name;
            Value = value;
        }

        public FieldNode(ExpressionNode index, ExpressionNode value)
            : base(NodeType.Field)
        {
            Subtype = NodeSubtype.IndexExpression;
            IndexExpression = index;
            Value =value;
        }

        public FieldNode(ExpressionNode value)
            : base(NodeType.Field)
        {
            Subtype = NodeSubtype.Value;
            Value = value;
        }
    }

    class ExpressionListNode : Node
    {
        public List<ExpressionNode> ExpList { get; private set; }
        public ExpressionListNode(List<ExpressionNode> expList)
            : base(NodeType.ExpressionList)
        {
            ExpList = expList;
        }
    }

    class TableConstructorNode : Node
    {
        public FieldListNode FieldList { get; private set; }
        public TableConstructorNode(FieldListNode fieldList)
            : base(NodeType.TableConstructor)
        {
            FieldList = fieldList;
        }
    }

    class FieldListNode : Node
    {
        public List<FieldNode> List { get; private set; }
        public FieldListNode(List<FieldNode> list)
                    : base(NodeType.FieldList)
        {
            List = list;
        }
    }

    class StringNode: Node
    {
        public Token StringArg { get; private set; }
        public StringNode(Token str)
            : base(NodeType.String)
        {
            StringArg = str;
        }
    }

    class FunctionCallStatementNode : StatementNode
    {
        public Node VarOrExp { get; private set; }
        public List<NameAndArgsNode> NameAndArgsList { get; private set; }
        public FunctionCallStatementNode(Node varOrExp, List<NameAndArgsNode> nameAndArgsList)
            : base(NodeType.FunctionCall)
        {
            Debug.Assert(varOrExp.Type == NodeType.Variable || varOrExp.Type == NodeType.Expression);
            VarOrExp = varOrExp;
            NameAndArgsList = nameAndArgsList;
        }
    }

    class IfStatementNode : StatementNode
    {
        public ExpressionNode Expression { get; private set; }
        public BlockNode Then { get; private set; }
        public List<ElseIfNode> ElseIfList { get; private set; }
        public BlockNode Else { get; private set; }

        public IfStatementNode(ExpressionNode exp, BlockNode thenBlk,
                                List<ElseIfNode> elseIfList, BlockNode elseBlk)
            :base(NodeType.If)
        {
            Expression = exp;
            Then = thenBlk;
            ElseIfList = elseIfList;
            Else = elseBlk;
        }
    }

    class AssignmentNode : StatementNode
    {
        public List<VariableNode> VarList { get; private set; }
        public List<ExpressionNode> ExpList { get; private set; }
        public AssignmentNode(List<VariableNode> varList, List<ExpressionNode> expList)
            :base(NodeType.Assignment)
        {
            VarList = varList;
            ExpList = expList;
        }
    }

    class ForStatementNode : StatementNode
    {
        public Token Name { get; private set; }
        public ExpressionNode Init { set; private get; }
        public ExpressionNode Test { set; private get; }
        public ExpressionNode Inc { set; private get; }
        public BlockNode Block { set; private get; }
        public ForStatementNode(Token name, ExpressionNode init,
                                ExpressionNode test, ExpressionNode inc,
                                BlockNode block)
            : base(NodeType.For)
        {
            Name = name;
            Init = init;
            Test = test;
            Inc = inc;
        }
    }

    class ForEachStatementNode : StatementNode
    {
        public List<Token> NameList { get; private set; }
        public ExpressionListNode ExpressionList { get; private set; }
        public BlockNode Block { get; private set; }
        public ForEachStatementNode(List<Token> nameList, ExpressionListNode expList,
                                    BlockNode block)
            : base(NodeType.ForEach)
        {
            NameList = nameList;
            ExpressionList = expList;
            Block = block;
        }
    }

    class FunctionStatementNode : StatementNode
    {
        public Token Name { get; private set; }
        public FunctionBodyNode Body { get; private set; }
        public FunctionStatementNode(Token name, FunctionBodyNode body)
            :base(NodeType.Function)
        {
            Name = name;
            Body = body;
        }
    }

    class FunctionNameNode : Node
    {
        public Token Name { get; private set; }
        public List<Token> DotList { get; private set; }
        public Token ColonName { get; private set; }
        public FunctionNameNode(Token name, List<Token> dotList, Token colonName)
            :base(NodeType.FunctionName)
        {
            Name = name;
            DotList = dotList;
            ColonName = colonName;
        }
    }

    class FunctionBodyNode : Node
    {
        public List<Token> ParameterList { get; private set; }
        public BlockNode Block { get; private set; }
        public FunctionBodyNode(List<Token> parameterList, BlockNode block)
            : base(NodeType.FunctionBody)
        {
            ParameterList = parameterList;
            Block = block;  
        }
    }

    class ParameterListNode : Node
    {
        public List<Token> List { get; private set; }
        public ParameterListNode(List<Token> list)
            : base(NodeType.ParameterList)
        {
            List = list;
        }
    }

    class LocalFunctionStatementNode : StatementNode
    {
        public Token Name { get; private set; }
        public FunctionBodyNode Body { get; private set; }
        public LocalFunctionStatementNode(Token name, FunctionBodyNode body)
            : base(NodeType.LocalFunction)
        {
            Name = name;
            Body = body;
        }
    }

    class LocalVariableDeclarationNode: StatementNode
    {
        public List<Token> NameList { get; private set; }
        public ExpressionListNode ExpressionList { get; private set; }
        public LocalVariableDeclarationNode(List<Token> nameList, ExpressionListNode expList)
            :base(NodeType.LocalVariableDeclaration)
        {
            NameList = nameList;
            ExpressionList = expList;
        }
    }

    class ExpressionNode : Node
    {
        public NodeSubtype Subtype { get; private set; }
        public Token TokenValue { get; private set; }
        public Token Operator { get; private set; }
        public ExpressionNode Lhs { get; private set; }
        public ExpressionNode Rhs { get; private set; }
        public ExpressionNode(Token t)
            : base(NodeType.Expression)
        {
            Subtype = NodeSubtype.SimpleValue;
            TokenValue = t;
        }
        public ExpressionNode(Token binOp, ExpressionNode lhs, ExpressionNode rhs)
            :base(NodeType.Expression)
        {
            Subtype = NodeSubtype.Operator;
            Operator = binOp;
            Lhs = lhs;
            Rhs = rhs;
        }

        public ExpressionNode(Token unOp, ExpressionNode rhs)
            :base(NodeType.Expression)
        {
            Subtype = NodeSubtype.Operator;
            Operator = unOp;
            Rhs = rhs;
        }
    }

    class VariableSuffixNode : Node
    {
        public NodeSubtype Subtype { get; private set; }
        public List<NameAndArgsNode> NameAndArgsList { get; private set; }
        public ExpressionNode IndexExpression { get; private set; }
        public Token DotName { get; private set; }
        public VariableSuffixNode(List<NameAndArgsNode> nameAndArgsList, ExpressionNode indexExpression)
            :base(NodeType.VariableSuffix)
        {
            Subtype = NodeSubtype.Indexed;
            NameAndArgsList = nameAndArgsList;
            IndexExpression = indexExpression;
        }

        public VariableSuffixNode(List<NameAndArgsNode> nameAndArgsList, Token dotName)
            : base(NodeType.VariableSuffix)
        {
            Subtype = NodeSubtype.DotNamed;
            NameAndArgsList = nameAndArgsList;
            DotName = dotName;
        }
    }

    class VariableNode : Node
    {
        public Token Name { get; private set; }
        public ExpressionNode Expression { get; private set; }
        public List<VariableSuffixNode> VarSuffixList { get; private set; }

        public VariableNode(ExpressionNode expression, List<VariableSuffixNode> varSuffixList)
            : base(NodeType.Variable)
        {
            Expression = expression;
            VarSuffixList = varSuffixList;
        }

        public VariableNode(Token name, List<VariableSuffixNode> varSuffixList)
            : base(NodeType.Variable)
        {
            Name = name;
            VarSuffixList = varSuffixList;
        }
    }

    class FunctionDefinitionNode : Node
    {
        public ParameterListNode ParameterList { get; private set; }
        public BlockNode Block { get; private set; }
        public FunctionDefinitionNode(ParameterListNode paramList, BlockNode block)
            : base(NodeType.FunctionDef)
        {
            ParameterList = paramList;
            Block = block;
        }
    }

    class PrefixExpressionNode: Node
    {
        public NodeSubtype Subtype { get; private set; }
        Node _varOrExp;
        public List<NameAndArgsNode> NameAndArgsList { get; private set; }
        public PrefixExpressionNode(Node varOrExp, List<NameAndArgsNode> nameAndArgs)
            :base(NodeType.PrefixExpression)
        {
            NameAndArgsList = nameAndArgs;
            _varOrExp = varOrExp;
            switch(varOrExp.Type)
            {
            case NodeType.Variable:
                Subtype = NodeSubtype.Variable;
                break;
            case NodeType.Expression:
                Subtype = NodeSubtype.Expression;
                break;
            }
        }
    }
}
