https://www.lua.org/manual/5.3/manual.html
With Modifications.
  1. Added the "assignment" statement rule.
  2. converted from the BNF to a more RE / Yacc syntax
  3. adopted the exp operator precedence rules from the antlr grammar.


chunk : block EOF ;
block : stat* retstat? ;
stat  : ';'
    | assignment
    | functioncall
    | label
    | 'break'
    | 'goto' Name
    | 'do' block 'end'
    | 'while' exp 'do' block 'end'
    | 'repeat' block 'until' exp
    | 'if' exp 'then' block ('elseif' exp 'then' block)* ('else' block)? 'end'
    | 'for' Name '=' exp ',' exp (',' exp)? 'do' block 'end'
    | 'for' namelist 'in' explist 'do' block 'end'
    | 'function' funcname funcbody
    | 'local' 'function' Name funcbody
    | 'local' namelist ('=' explist)?
    ;

assignment: varlist '=' explist ;

retstat   : 'return' (explist)? ';'? ;
label     : '::' Name '::' ;
funcname  : Name ('.' Name)* (':' Name)? ;
varlist   : var (',' var)* ;
var       : (NAME | '(' exp ')' varSuffix) varSuffix* ;
varSuffix : nameAndArgs* ('[' exp ']' | '.' NAME) ;
namelist  : Name (',' Name)* ;
explist   : exp (',' exp)* ;

exp       :  'nil' | 'false'| 'true'
        | Number | String | '...'
        | functiondef | prefixexp | tableconstructor
        | <assoc=right> exp operatorPower exp
        | operatorUnary exp
        | exp operatorMulDivMod exp
        | exp operatorAddSub exp
        | <assoc=right> exp operatorStrcat exp
        | exp operatorComparison exp
        | exp operatorAnd exp
        | exp operatorOr exp
        | exp operatorBitwise exp
        ;

prefixexp    : varOrExp nameAndArgs* ;
functioncall : varOrExp nameAndArgs+ ;
varOrExp     : var | '(' exp ')' ;
nameAndArgs  : (':' NAME)? args ;

args         :  '(' (explist)? ')' | tableconstructor | String ;
functiondef  : 'function' funcbody ;
funcbody     : '(' (parlist)? ')' block 'end' ;
parlist      : namelist (',' '...')? | '...' ;
tableconstructor : '{' (fieldlist)? '}' ;
fieldlist    : field (fieldsep field)* fieldsep? ;
field        : '[' exp ']' '=' exp | Name '=' exp | exp ;
fieldsep     : ',' | ';' ;

operatorOr         : 'or' ;
operatorAnd        : 'and' ;
operatorComparison : '<' | '>' | '<=' | '>=' | '~=' | '==' ;
operatorStrcat     : '..' ;
operatorAddSub     : '+' | '-' ;
operatorMulDivMod  : '*' | '/' | '%' | '//' ;
operatorBitwise    : '&' | '|' | '~' | '<<' | '>>' ;
operatorUnary      : 'not' | '#' | '-' | '~' ;
operatorPower      : '^' ;

Number : INT | HEX | FLOAT | HEX_FLOAT
String : NORMALSTRING | CHARSTRING | LONGSTRING

