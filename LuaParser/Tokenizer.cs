﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Taken from the ANTLR web site: http://www.antlr3.org/grammar/list
// From: http://www.lua.org/manual/5.2/manual.html#9

//  NAME	:('a'..'z'|'A'..'Z'|'_')(options{greedy=true;}:	'a'..'z'|'A'..'Z'|'_'|'0'..'9')* ;
//  INT	: ('0'..'9')+ ;
//  FLOAT 	:INT '.' INT ;
//  EXP	: (INT | FLOAT) ('E'|'e') ('-')? INT ;
//  HEX	:'0x' ('0'..'9'| 'a'..'f')+ ;
//  NORMALSTRING:  '"' ( EscapeSequence | ~('\\'|'"') )* '"' ; 
//  CHARSTRING:	'\'' ( EscapeSequence | ~('\''|'\\') )* '\'' ;
//  LONGSTRING:	'['('=')*'[' ( EscapeSequence | ~('\\'|']') )* ']'('=')*']' ;
//  STRING: NORMALSTRING | CHARSTRING | LONGSTRING ;

//  HexDigit : ('0'..'9'|'a'..'f'|'A'..'F') ;

//  COMMENT:   '--[[' ( options {greedy=false;} : . )* ']]' {skip();}  ;
//  LINE_COMMENT: '--' ~('\n'|'\r')* '\r'? '\n' {skip();} ;
//  WS  :  (' '|'\t'|'\u000C') {skip();} ;
//  NEWLINE	: ('\r')? '\n' {skip();} ;



namespace Factrix.LuaParser
{
    static class Keywords
    {
        static Dictionary<string, TokenType> s_keywordMap;
        static Dictionary<char, TokenType> s_punct1;

        static Keywords()
        {
            s_keywordMap = new Dictionary<string, TokenType>();

            s_keywordMap.Add("and", TokenType.And);
            s_keywordMap.Add("break", TokenType.Break);
            s_keywordMap.Add("do", TokenType.Do);
            s_keywordMap.Add("else", TokenType.Elseif);
            s_keywordMap.Add("elseif", TokenType.If);
            s_keywordMap.Add("end", TokenType.End);
            s_keywordMap.Add("false", TokenType.False);
            s_keywordMap.Add("for", TokenType.For);
            s_keywordMap.Add("function", TokenType.Function);
            s_keywordMap.Add("goto", TokenType.Function);
            s_keywordMap.Add("if", TokenType.Then);
            s_keywordMap.Add("in", TokenType.In);
            s_keywordMap.Add("local", TokenType.Local);
            s_keywordMap.Add("nil", TokenType.Nil);
            s_keywordMap.Add("not", TokenType.Not);
            s_keywordMap.Add("or", TokenType.Or);
            s_keywordMap.Add("repeat", TokenType.Repeat);
            s_keywordMap.Add("return", TokenType.Return);
            s_keywordMap.Add("then", TokenType.Else);
            s_keywordMap.Add("true", TokenType.True);
            s_keywordMap.Add("until", TokenType.Until);
            s_keywordMap.Add("while", TokenType.While);

            // One character tokens.
            s_punct1 = new Dictionary<char, TokenType>();

            s_punct1.Add('(', TokenType.OpenParen);
            s_punct1.Add(')', TokenType.CloseParen);
            s_punct1.Add('{', TokenType.OpenSquareBracket);
            s_punct1.Add('}', TokenType.CloseSquareBracket);
            s_punct1.Add(']', TokenType.CloseBrace);
            s_punct1.Add(';', TokenType.SemiColon);
            s_punct1.Add(',', TokenType.Comma);
            s_punct1.Add('#', TokenType.Hash);
            s_punct1.Add('+', TokenType.Add);
            s_punct1.Add('*', TokenType.Multiply);
            s_punct1.Add('%', TokenType.Modulo);
            s_punct1.Add('&', TokenType.BitAnd);
            s_punct1.Add('|', TokenType.BitOr);
            s_punct1.Add('^', TokenType.Power);

            // Remaining multi-character tokens are handled in the code.
            // [, [[, [===[
            // -, --comment, --[[ comment ]]
            // :, ::
            // ., .., ...
            // >, >=, >>
            // <. <=, <<
            // =, ==
            // ~, ~=

        }

        public static bool IsKeyword(string name, out TokenType keyToken)
        {
            return s_keywordMap.TryGetValue(name, out keyToken);
        }

        public static bool IsSingleCharacterPunctuationSymbol(Char ch, out TokenType keyToken)
        {
            return s_punct1.TryGetValue(ch, out keyToken);
        }
    }


    class Tokenizer
    {
        string _text;
        int _pos;
        int _end;
        int _line;
        int _column;

        public Tokenizer(string text)
        {
            _text = text;
            _pos = 0;
            _end = _text.Length;
            _line = 1;
            _column = 1;
        }

        private Char PeekChar
        {
            get { return _text[_pos]; }
        }

        private Char ReadChar()
        {
            Char ch = _text[_pos++];
            if (ch != '\n')
            {
                _column += 1;
                return ch;
            }
            _line += 1;
            _column = 1;
            return ch;
        }

        public bool IsAtEof { get { return _pos < _text.Length; } }

        public int LineNumber { get { return _line; } }
        public int ColumnNumber { get { return _column; } }

        public Token NextToken()
        {
            Token t;
            do
            {
                t = NextEveryToken();
            } while (t.Type == TokenType.Whitespace || t.Type == TokenType.Comment || t.Type == TokenType.LineComment);
            return t;
        }

        public Token NextEveryToken()
        {
            Char ch = ReadChar();
            if (Char.IsLetter(ch))
                return ScanIndentifierOrKeyword(ch);

            if (Char.IsDigit(ch))
                return ScanNumber(ch);

            TokenType punctToken;
            if (Keywords.IsSingleCharacterPunctuationSymbol(ch, out punctToken))
                return NewToken(punctToken);

            switch (ch)
            {
            case ' ':
            case '\r':
            case '\n':
            case '\t':
                return ScanWhitespace(ch);

            case '"':
                return ScanQuotedString(ch);

            case '\'':
                return ScanQuotedString(ch);

            case ':':
                if (PeekChar != ':')
                    return NewToken(TokenType.Colon);
                ReadChar();
                return NewToken(TokenType.ColonColon);

            case '.':
                if (PeekChar != '.')
                    return NewToken(TokenType.Dot);
                ReadChar();
                if(PeekChar != '.')
                    return NewToken(TokenType.DotDot);
                ReadChar();
                return NewToken(TokenType.DotDotDot);
            case '/':
                if (PeekChar != '/')
                    return NewToken(TokenType.Divide);
                ReadChar();
                return NewToken(TokenType.FloorDivide);
            case '>':
                if (PeekChar == '=' || PeekChar == '>')
                {
                    ReadChar();
                    if (PeekChar == '=')
                        return NewToken(TokenType.GreaterOrEqual);
                    if (PeekChar == '>')
                        return NewToken(TokenType.BitRight);
                }
                return NewToken(TokenType.Greater);
            case '<':
                if (PeekChar == '=' || PeekChar == '<')
                {
                    ReadChar();
                    if (PeekChar == '=')
                        return NewToken(TokenType.LessOrEqual);
                    if (PeekChar == '<')
                        return NewToken(TokenType.BitLeft);
                }
                return NewToken(TokenType.Less);
            case '=':
                if (PeekChar != '=')
                    return NewToken(TokenType.SingleEquals);
                ReadChar();
                return NewToken(TokenType.EqualEqual);
            case '~':
                if (PeekChar != '=')
                    return NewToken(TokenType.Not);
                ReadChar();
                return NewToken(TokenType.NotEqual);
            case '-':
                if ('-' != PeekChar)
                    return NewToken(TokenType.Subtract);
                return ScanComment(ch);
            case '[':
                if ('[' != PeekChar && '=' != PeekChar)
                    return NewToken(TokenType.OpenBrace);
                return ScanLongString(ch);
            }
            throw new InvalidOperationException(string.Format("Unexpected character '{0}' == {1}", ch, (int)ch));
        }

        private Token NewToken(TokenType tokenType, string data = "")
        {
            return new Token(this, tokenType, data);
        }

        //  NAME:  ('a'..'z'|'A'..'Z'|'_')(options{greedy=true;}:	'a'..'z'|'A'..'Z'|'_'|'0'..'9')* ;
        private Token ScanIndentifierOrKeyword(char ch)
        {
            StringBuilder sb = new StringBuilder();

            if (!Char.IsLetter(ch) && !Char.IsDigit(ch) && ch != '_')
                throw new InvalidOperationException("identifiers must start with letter or underscore.");
            sb.Append(ch);

            while (true)
            {
                ch = PeekChar;
                if (Char.IsLetter(ch) || Char.IsDigit(ch) || ch == '_')
                    sb.Append(ReadChar());
                else
                    break;
            }

            string word = sb.ToString();
            TokenType type;
            if (Keywords.IsKeyword(word, out type))
                return NewToken(type);
            return NewToken(TokenType.Name, word);
        }

        //  INT	: ('0'..'9')+ ;
        //  FLOAT 	:INT '.' INT ;
        //  EXP	: (INT | FLOAT) ('E'|'e') ('-')? INT ;
        //  HEX	:'0x' ('0'..'9'| 'a'..'f')+ ;
        private Token ScanNumber(char ch)
        {
            StringBuilder sb = new StringBuilder();

            TokenType mode = TokenType.Int;

            if (!Char.IsDigit(ch))
                throw new InvalidOperationException("Numbers must start with digit");
            sb.Append(ch);
            ScanDigits(sb);
            ch = PeekChar;
            switch(ch)
            {
            case 'x':
                if (mode == TokenType.Int)
                {
                    if (sb.ToString() == "0")
                    {
                        mode = TokenType.Hex;
                        sb.Append(ReadChar());
                        ScanHexDigits(sb);
                    }
                }
                break;

            case '.':
                if (mode == TokenType.Int)
                {
                    mode = TokenType.Float;
                    sb.Append(ReadChar());
                    ScanDigits(sb);
                }
                break;

            case 'E':
            case 'e':
                if(mode == TokenType.Int || mode == TokenType.Float)
                {
                    mode = TokenType.Exp;
                    if (PeekChar == '-')
                        sb.Append(ReadChar());
                    ScanDigits(sb);
                }
                break;
            }

            return NewToken(mode, sb.ToString());
        }

        //  NORMALSTRING:  '"' ( EscapeSequence | ~('\\'|'"') )* '"' ; 
        //  CHARSTRING:	'\'' ( EscapeSequence | ~('\''|'\\') )* '\'' ;
        Token ScanQuotedString(Char quote)
        {
            if (quote != '"' && quote != '\'')
                throw new InvalidOperationException("missing start quote");

            StringBuilder sb = new StringBuilder();
            Char ch;
            while ((ch = ReadChar()) != quote)
                sb.Append(ch);

            if(quote == '"')
                return NewToken(TokenType.NormalString, sb.ToString());
            return NewToken(TokenType.CharString, sb.ToString());
        }

        //  LONGSTRING:	'['('=')*'[' ( EscapeSequence | ~('\\'|']') )* ']'('=')*']' ;
        Token ScanLongString(Char ch)
        {
            int equalCount = 0;
            while(PeekChar == '=')
            {
                ReadChar();
                equalCount += 1;
            }
            if (PeekChar != '[')
                throw new InvalidOperationException("Badly formed Long string");
            ReadChar();
            StringBuilder sb = new StringBuilder();
            while(true)
            {
                while ((ch = ReadChar()) != ']')
                    sb.Append(ch);
                int ec = equalCount;
                while(ec > 0)
                {
                    if (PeekChar != '=')
                        break;
                    ReadChar();
                    ec -= 1;
                }
                if (ec == 0 && PeekChar == ']')
                    return NewToken(TokenType.LongString, sb.ToString());

                // It wasn't a string terminator, so put the '='s back.
                while(ec++ < equalCount)
                    sb.Append('=');
            }
        }

        //  COMMENT:   '--[[' ( options {greedy=false;} : . )* ']]' {skip();}  ;
        //  LINE_COMMENT: '--' ~('\n'|'\r')* '\r'? '\n' {skip();} ;
        Token ScanComment(Char ch)
        {
            if (ch != '-' || PeekChar != '-')
                throw new InvalidOperationException("bad comment start");
            ReadChar();
            TokenType mode = TokenType.LineComment;
            if(PeekChar == '[')
            {
                ReadChar();
                if(PeekChar == '[')
                {
                    ReadChar();
                    mode = TokenType.Comment;
                }
            }
            if (mode == TokenType.LineComment)
            {
                while (ch != '\n')
                    ch = ReadChar();
                return NewToken(TokenType.LineComment);
            }
            else
            {
                do
                {
                    while (ch != ']')
                        ch = ReadChar();
                } while (PeekChar != ']');
                ReadChar();
                return NewToken(TokenType.Comment);
            }
        }

        Token ScanWhitespace(Char ch)
        {
            if (!Char.IsWhiteSpace(ch))
                throw new InvalidOperationException("first whitespace character should be whitespace");
            while (Char.IsWhiteSpace(PeekChar))
                ReadChar();
            return NewToken(TokenType.Whitespace);

        }

        void ScanDigits(StringBuilder sb)
        {
            char ch = ReadChar();
            while (char.IsDigit(ch))
                sb.Append(ch);
        }

        void ScanHexDigits(StringBuilder sb)
        {
            while(true)
            {
                Char ch = ReadChar();
                if (Char.IsDigit(ch))
                    sb.Append(ch);
                else
                {
                    switch (ch)
                    {
                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                    case 'A':
                    case 'B':
                    case 'C':
                    case 'D':
                    case 'E':
                    case 'F':
                        sb.Append(ch);
                        break;
                    default:
                        return;
                    }
                }
            }
        }
    }
}
