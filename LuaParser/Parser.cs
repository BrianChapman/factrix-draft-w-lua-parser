﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Factrix.LuaParser
{

    class Parser
    {
        Tokenizer _tokenizer;
        Token _currentToken;

        public Parser(string text)
        {
            _tokenizer = new Tokenizer(text);
        }

        //chunk : block EOF ;
        public BlockNode Parse()
        {
            ReadToken();
            BlockNode node = ParseBlock();
            MustBeAtEof();
            return node;
        }

        private void MustMatch(Token t, TokenType type, string content = "")
        {
            if (t.Type != type)
                throw new InvalidOperationException(
                    string.Format("{0} MustMatch Type failed.  Have '{1}', want '{2}'",
                                LineInfoString(t), t.Type, type));
            if (!string.IsNullOrEmpty(content) && t.Content != content)
                throw new InvalidOperationException(
                    string.Format("{0} MustMatch Data failed.  Have '{1}', want '{2}'",
                                LineInfoString(t), t.Content, content));
        }

        void CurrentMustMatch(TokenType type, string content = "")
        {
            MustMatch(_currentToken, type, content);
        }

        Token CurrentToken
        {
            get { return _currentToken; }
        }

        Token ReadToken()
        {
            _currentToken = _tokenizer.NextToken();
            return _currentToken;
        }

        Token NextMustBe(TokenType expected)
        {
            _currentToken = _tokenizer.NextToken();
            MustMatch(_currentToken, expected);
            return _currentToken;
        }

        static string LineInfoString(Token t)
        {
            return string.Format("({0},{1})", t.Line, t.Column);
        }

        void MustBeAtEof()
        {
            Debug.Assert(_tokenizer.IsAtEof);
        }

        // Easy LR0 Parsing of:  Rule?
        Node Parse_QuestionMark(Func<Token, bool> IsPrefixToken,
                                Func<Node> ParseNode)
        {
            if (IsPrefixToken(CurrentToken))
                return ParseNode();
            return null;
        }

        // Easy LR0 Parsing of:  Rule+
        List<Node> Parse_Plus(Func<Token, bool> IsPrefixToken,
                              Func<Node> ParseNode,
                              Func<Token, bool> IsListSeperator,
                              List<Node> list = null)
        {
            Debug.Assert(IsPrefixToken(CurrentToken));
            if (list == null)
                list = new List<Node>();
            Node node = ParseNode();
            list.Add(node);
            if (IsListSeperator == null || IsListSeperator(CurrentToken))
            {
                // there may not me a seperator.
                if (IsListSeperator != null)
                    ReadToken();
                list = Parse_Star(IsPrefixToken, ParseNode, IsListSeperator, list);
            }
            return list;
        }

        // Easy LR0 Parsing of:  Rule*
        List<Node> Parse_Star(Func<Token, bool> IsPrefixToken,
                                      Func<Node> ParseNode,
                                      Func<Token, bool> IsListSeperator,
                                      List<Node> list = null)
        {
            if (list == null)
                list = new List<Node>();
            Node node = null;
            while (IsPrefixToken(CurrentToken))
            {
                Debug.Assert(IsPrefixToken(CurrentToken));
                node = ParseNode();
                list.Add(node);

                // there may not me a seperator.
                if (IsListSeperator != null)
                {
                    if (!IsListSeperator(CurrentToken))
                        break;
                }
            }
            return (list != null) ? list : null;
        }

        static public List<T> ConvertNodeList<T>(List<Node> nodeList)
            where T : Node
        {
            List<T> list = new List<T>();
            foreach (Node n in nodeList)
                list.Add((T)n);
            return list;
        }




        //block : stat* retstat? ;
        public BlockNode ParseBlock()
        {
            List<Node> list = Parse_Star(IsPrefix_Statment, ParseStatement, null);
            Node returnStatement = Parse_QuestionMark(IsPrefix_ReturnStatement, ParseReturnStatement);

            if(returnStatement != null)
                list.Add(returnStatement);

            var statementList = ConvertNodeList<StatementNode>(list);
            BlockNode block = new BlockNode(statementList);
            return block;
        }

        //retstat: 'return' explist? ';'?
        bool IsPrefix_ReturnStatement(Token t)
        {
            return CurrentToken.Type == TokenType.Return;
        }
        StatementNode ParseReturnStatement()
        {
            CurrentMustMatch(TokenType.Return);
            ReadToken();

            ExpressionNode expNode = null;
            if (CurrentToken.Type == TokenType.SemiColon)
                ReadToken();
            else
                expNode = ParseExpression();
            return new ReturnStatementNode(expNode);

        }

        //stat
        //    : ';'
        //    | assignment
        //    | functioncall
        //    | label
        //    | 'break'
        //    | 'goto' NAME
        //    | 'do' block 'end'
        //    | 'while' exp 'do' block 'end'
        //    | 'repeat' block 'until' exp
        //    | 'if' exp 'then' block('elseif' exp 'then' block)* ('else' block)? 'end'
        //    | 'for' NAME '=' exp ',' exp(',' exp)? 'do' block 'end'
        //    | 'for' namelist 'in' explist 'do' block 'end'
        //    | 'function' funcname funcbody
        //    | 'local' 'function' NAME funcbody
        //    | 'local' namelist('=' explist)?
        //    ;
        bool IsPrefix_Statment(Token t)
        {
            switch (t.Type)
            {
            case TokenType.Do:
            case TokenType.While:
            case TokenType.Break:
            case TokenType.Repeat:
            case TokenType.If:
            case TokenType.For:
            case TokenType.Function:
            case TokenType.Local:
            case TokenType.SemiColon:
            case TokenType.Name:
            case TokenType.OpenParen:
                return true;
            default:
                return false;
            }
        }

        private StatementNode ParseStatement()
        {
            switch (CurrentToken.Type)
            {
            //    : ';'
            case TokenType.SemiColon:
                return new StatementNode(NodeType.Empty);

            //  assignment
            //  functioncall
            case TokenType.Name:
            case TokenType.OpenParen:
                return ParseAssignmentOrFunctionCall();

            //  label
            case TokenType.ColonColon:
                return ParseLabel();

            //  'break'
            case TokenType.Break:
                return ParseBreakStatement();

            //  'goto' NAME
            case TokenType.Goto:
                return ParseGotoStatement();

            //  'do' block 'end'
            case TokenType.Do:
                return ParseDoStatement();

            //  'while' exp 'do' block 'end'
            case TokenType.While:
                return ParseWhileStatement();

            //  'repeat' block 'until' exp
            case TokenType.Repeat:
                return ParseRepeatStatement();

            //  'if' exp 'then' block('elseif' exp 'then' block)* ('else' block)? 'end'
            case TokenType.If:
                return ParseIfStatement();

            //  'for' NAME '=' exp ',' exp(',' exp)? 'do' block 'end'
            //  'for' namelist 'in' explist 'do' block 'end'
            case TokenType.For:
                return ParseForStatements();

            //  'function' funcname funcbody
            case TokenType.Function:
                return ParseFunctionStatement();

            //  'local' 'function' NAME funcbody
            //  'local' namelist('=' explist)?
            case TokenType.Local:
                return ParseLocalStatement();

            default:
                throw new InvalidOperationException("missing case in ParseStatement");
            }
        }

        //   '::' Name '::'
        LabelStatementNode ParseLabel()
        {
            CurrentMustMatch(TokenType.ColonColon);
            NextMustBe(TokenType.Name);
            LabelStatementNode node = new LabelStatementNode(CurrentToken);
            NextMustBe(TokenType.ColonColon);
            return node;
        }

        //   'break'
        StatementNode ParseBreakStatement()
        {
            CurrentMustMatch(TokenType.Break);
            ReadToken();
            return new StatementNode(NodeType.Break);
        }

        //   'goto' NAME
        GotoStatementNode ParseGotoStatement()
        {
            CurrentMustMatch(TokenType.Goto);
            NextMustBe(TokenType.Name);
            GotoStatementNode node = new GotoStatementNode(CurrentToken);
            return node;
        }

        //   'do' block 'end'
        DoStatementNode ParseDoStatement()
        {
            CurrentMustMatch(TokenType.Do);
            BlockNode block = ParseBlock();
            NextMustBe(TokenType.End);
            DoStatementNode node = new DoStatementNode(block);
            return node;
        }

        //   'while' exp 'do' block 'end'
        WhileStatementNode ParseWhileStatement()
        {
            CurrentMustMatch(TokenType.Do);
            ExpressionNode exp = ParseExpression();
            NextMustBe(TokenType.Do);
            BlockNode block = ParseBlock();
            NextMustBe(TokenType.End);
            var node = new WhileStatementNode(exp, block);
            return node;
        }

        //  'repeat' block 'until' exp
        RepeatStatementNode ParseRepeatStatement()
        {
            CurrentMustMatch(TokenType.Repeat);
            BlockNode block = ParseBlock();
            NextMustBe(TokenType.Until);
            ExpressionNode exp = ParseExpression();
            var node = new RepeatStatementNode(exp, block);
            return node;
        }

        //  'if' exp 'then' block ('elseif' exp 'then' block)* ('else' block)? 'end'
        IfStatementNode ParseIfStatement()
        {
            BlockNode elseBlock = null;
            List<ElseIfNode> elseIfList = null;

            CurrentMustMatch(TokenType.If);
            ExpressionNode exp = ParseExpression();
            NextMustBe(TokenType.Then);
            BlockNode thenBlock = ParseBlock();
            while(CurrentToken.Type == TokenType.Elseif)
            {
                ExpressionNode innerExp = ParseExpression();
                NextMustBe(TokenType.Then);
                BlockNode innerBlock = ParseBlock();
                var innerPair = new ElseIfNode(innerExp, innerBlock);
                elseIfList.Add(innerPair);
            }
            if(CurrentToken.Type == TokenType.Else)
            {
                elseBlock = ParseBlock();
            }
            NextMustBe(TokenType.End);
            var node = new IfStatementNode(exp, thenBlock, elseIfList, elseBlock);
            return node;
        }

        //  'for' NAME '=' exp ',' exp(',' exp)? 'do' block 'end'
        //  'for' namelist 'in' explist 'do' block 'end'
        StatementNode ParseForStatements()
        {
            CurrentMustMatch(TokenType.For);
            Token name = NextMustBe(TokenType.Name);
            ReadToken();
            if (CurrentToken.Type == TokenType.SingleEquals)
                return ParseForStatement(name);
            return ParseForEachStatement(name);
        }

        //  'for' NAME '=' exp ',' exp(',' exp)? 'do' block 'end'
        ForStatementNode ParseForStatement(Token name)
        {
            CurrentMustMatch(TokenType.SingleEquals);
            ReadToken();
            ExpressionNode init = ParseExpression();
            CurrentMustMatch(TokenType.Comma);
            ReadToken();
            ExpressionNode test = ParseExpression();
            ExpressionNode inc = null;
            if (CurrentToken.Type == TokenType.Comma)
            {
                ReadToken();
                inc = ParseExpression();
            }
            CurrentMustMatch(TokenType.Do);
            ReadToken();
            BlockNode block = ParseBlock();
            CurrentMustMatch(TokenType.End);
            ReadToken();
            var node = new ForStatementNode(name, init, test, inc, block);
            return node;
        }

        //  'for' namelist 'in' explist 'do' block 'end'
        ForEachStatementNode ParseForEachStatement(Token name)
        {
            // we having already read the first "Name"
            List<Token> nameList = ParseNameList(name);
            CurrentMustMatch(TokenType.In);
            ReadToken();
            ExpressionListNode expList = ParseExpressionList();
            CurrentMustMatch(TokenType.Do);
            ReadToken();
            BlockNode block = ParseBlock();
            CurrentMustMatch(TokenType.End);
            ReadToken();
            var node = new ForEachStatementNode(nameList, expList, block);
            return node;
        }

        //  'function' funcname funcbody
        StatementNode ParseFunctionStatement()
        {
            CurrentMustMatch(TokenType.Function);
            FunctionNameNode funcName = ParseFunctionName();
            Token name = NextMustBe(TokenType.Name);
            ReadToken();
            FunctionBodyNode funcBody = ParseFunctionBody();
            var node = new FunctionStatementNode(name, funcBody);
            return node;
        }

        // funcname  : Name('.' Name)* (':' Name)? ;
        FunctionNameNode ParseFunctionName()
        {
            Token colonName = null;
            List<Token> dotList = null;

            CurrentMustMatch(TokenType.Name);
            Token name = CurrentToken;
            ReadToken();
            if (CurrentToken.Type == TokenType.Dot)
                dotList = new List<Token>();
            while (CurrentToken.Type == TokenType.Dot)
            {
                ReadToken();
                CurrentMustMatch(TokenType.Name);
                dotList.Add(CurrentToken);
            }
            if(CurrentToken.Type == TokenType.Colon)
            {
                ReadToken();
                colonName = NextMustBe(TokenType.Name);
            }
            var node = new FunctionNameNode(name, dotList, colonName);
            return node;
        }

        // funcbody   : '(' (parlist)? ')' block 'end' ;
        FunctionBodyNode ParseFunctionBody()
        {
            CurrentMustMatch(TokenType.OpenParen);
            Node paramListNode = Parse_QuestionMark(IsPrefix_ParameterList,
                                                    ParseParameterList);
            NextMustBe(TokenType.CloseParen);

            BlockNode block = ParseBlock();
            NextMustBe(TokenType.End);
            ReadToken();
            List<Token> paramList = ((ParameterListNode)paramListNode).List;
            var node = new FunctionBodyNode(paramList, block);
            return node;
        }

        // parlist      : namelist (',' '...')? | '...' ;
        bool IsPrefix_ParameterList(Token t)
        {
            return t.Type == TokenType.Name
                || t.Type == TokenType.DotDotDot;
        }
        ParameterListNode ParseParameterList()
        {
            List<Token> paramList = new List<Token>();

            switch(CurrentToken.Type)
            {
            // simple:  '...'
            case TokenType.DotDotDot:
                paramList.Add(CurrentToken);
                break;

            // or:  namelist (',' '...')?
            case TokenType.Name:
                paramList = ParseNameList();
                if(CurrentToken.Type == TokenType.Comma)
                {
                    Token t = NextMustBe(TokenType.DotDotDot);
                    paramList.Add(t);
                }
                break;

            default:
                Debug.Assert(null == "Bad case in ParseParameterList");
                break;
            }
            var node = new ParameterListNode(paramList);
            return node;
        }

        //  'local' 'function' NAME funcbody
        //  'local' namelist('=' explist)?
        StatementNode ParseLocalStatement()
        {
            CurrentMustMatch(TokenType.Local);
            ReadToken();
            if(CurrentToken.Type == TokenType.Function)
                return ParseLocalFunctionStatement();
            return ParseLocalVariableDeclaration();
        }

        //  'local' 'function' NAME funcbody
        LocalFunctionStatementNode ParseLocalFunctionStatement()
        {
            CurrentMustMatch(TokenType.Function);
            Token name = NextMustBe(TokenType.Name);
            FunctionBodyNode funcBody = ParseFunctionBody();
            var node = new LocalFunctionStatementNode(name, funcBody);
            return node;
        }

        //  'local' namelist('=' explist)?
        LocalVariableDeclarationNode ParseLocalVariableDeclaration()
        {
            Token name = NextMustBe(TokenType.Name);
            List<Token> nameList = ParseNameList();
            ExpressionListNode expList = null;
            if (CurrentToken.Type == TokenType.SingleEquals)
            {
                ReadToken();
                expList = ParseExpressionList();
            }
            var node = new LocalVariableDeclarationNode(nameList, expList);
            return node;
        }

        // assignment   : varlist '=' explist ;
        // functioncall : varOrExp nameAndArgs+ ;
        // varOrExp     : var | '(' exp ')' ;
        // var          : (NAME | '(' exp ')' varSuffix) varSuffix* ;
        // nameAndArgs  : (':' NAME)? args ;
        // varSuffix    : nameAndArgs* ('[' exp ']' | '.' NAME) ;

        // Complex Look-ahead problem.
        // "assignment" starts with "var"
        // "functioncall" starts with either "var" or '(' exp ')'
        // But '(' exp ')' is also a legal start of "var"
        // Strategy:
        //  if it starts with "Name" then read a "var" and see where we are.
        //  if it starts with '(' read the "exp" ')' and see where we are.
        private StatementNode ParseAssignmentOrFunctionCall()
        {
            ExpressionNode expression = null;
            VariableNode variable = null;

            if (CurrentToken.Type == TokenType.OpenParen)
            {
                ReadToken();
                expression = ParseExpression();
                CurrentMustMatch(TokenType.CloseParen);
                if (IsPrefix_NameAndArgs(CurrentToken))
                {
                    // early out on the case of a functioncall starting with '(' exp ')'
                    return ParseFunctionCall(expression);
                }
                variable = ParseVariable(expression);
            }
            else
            {
                CurrentMustMatch(TokenType.Name);
                variable = ParseVariable(CurrentToken);
            }
            // At this point we have a leading "var" on what ever this is.
            if ((CurrentToken.Type == TokenType.SingleEquals) || (CurrentToken.Type == TokenType.Comma))
                return ParseAssignment(variable);
            return ParseFunctionCall(variable);
        }

        // var  : (NAME | '(' exp ')' varSuffix) varSuffix* ;
        bool IsPrefix_Variable(Token t)
        {
            return t.Type == TokenType.Name || t.Type == TokenType.OpenParen;
        }
        VariableNode ParseVariable()
        {
            Debug.Assert(IsPrefix_Variable(CurrentToken));
            switch(CurrentToken.Type)
            {
            case TokenType.Name:
                {
                    Token name = CurrentToken;
                    ReadToken();
                    return ParseVariable(name);
                }

            case TokenType.OpenParen:
                {
                    ReadToken();
                    ExpressionNode exp = ParseExpression();
                    return ParseVariable(exp);
                }

            default:
                Debug.Assert(null == "Bad Case in ParseVariable()");
                return null;
            }
        }

        // var : (NAME | '(' exp ')' varSuffix) varSuffix* ;
        // When we already read the first "NAME"
        VariableNode ParseVariable(Token name)
        {
            var list = Parse_Star(IsPrefix_VariableSuffix, ParseVariableSuffix, null);
            var suffixList = ConvertNodeList<VariableSuffixNode>(list);
            var node = new VariableNode(name, suffixList);
            return node;
        }

        // var : (NAME | '(' exp ')' varSuffix) varSuffix* ;
        // When we already red the first "Expression"
        VariableNode ParseVariable(ExpressionNode exp)
        {
            var list = Parse_Star(IsPrefix_VariableSuffix, ParseVariableSuffix, null);
            var sufList = ConvertNodeList<VariableSuffixNode>(list);
            var node = new VariableNode(exp, sufList);
            return node;
        }

        // varSuffix : nameAndArgs* ('[' exp ']' | '.' NAME) ;
        bool IsPrefix_VariableSuffix(Token t)
        {
            return (t.Type == TokenType.OpenSquareBracket
                || t.Type == TokenType.Dot
                || IsPrefix_NameAndArgs(t));
        }
        Node ParseVariableSuffix()
        {
            ExpressionNode exp = null;
            Token dotName = null;
            List<Node> list = Parse_Star(IsPrefix_NameAndArgs, ParseNameAndArgs, null);
            var nameAndArgList = ConvertNodeList<NameAndArgsNode>(list);
            switch (CurrentToken.Type)
            {
            case TokenType.OpenSquareBracket:
                {
                    exp = ParseExpression();
                    NextMustBe(TokenType.CloseSquareBracket);
                    ReadToken();
                    var node = new VariableSuffixNode(nameAndArgList, exp);
                    return node;
                }
            case TokenType.Dot:
                {
                    dotName = CurrentToken;
                    ReadToken();
                    var node = new VariableSuffixNode(nameAndArgList, dotName);
                    return node;
                }
            default:
                Debug.Assert(null == "Bad Case in ParseVariableSuffix");
                return null;
            }
        }

        // nameAndArgs  : (':' NAME)? args ;
        bool IsPrefix_NameAndArgs(Token t)
        {
            return (t.Type == TokenType.Colon || IsPrefix_Args(t));
        }
        NameAndArgsNode ParseNameAndArgs()
        {
            Debug.Assert(IsPrefix_NameAndArgs(CurrentToken));
            Token colonName = null;
            if(CurrentToken.Type == TokenType.Colon)
            {
                colonName = CurrentToken;
                ReadToken();
            }
            ArgsNode args = ParseArgs();
            var node = new NameAndArgsNode(colonName, args);
            return node;
        }

        // args  :  '(' (explist)? ')' | tableconstructor | String ;
        bool IsPrefix_Args(Token t)
        {
            return t.Type == TokenType.OpenParen
                || IsString(t)
                || IsPrefix_TableConstructor(t);
        }
        ArgsNode ParseArgs()
        {
            if (CurrentToken.Type == TokenType.OpenParen)
            {
                ReadToken();
                ExpressionListNode expList = null;
                if (IsPrefix_ExpressionList(CurrentToken))
                    expList = ParseExpressionList();
                CurrentMustMatch(TokenType.CloseParen);
                var node = new ArgsNode(expList);
                return node;
            }
            else if (IsString(CurrentToken))
            {
                var stringNode = new StringNode(CurrentToken);
                var node = new ArgsNode(stringNode);
                return node;
            }
            else if(IsPrefix_TableConstructor(CurrentToken))
            {
                var tableCtor = ParseTableConstructor();
                var node = new ArgsNode(tableCtor);
                return node;
            }
            Debug.Assert(null == "Bad case in ParseArgs");
            return null;
        }

        // explist   : exp(',' exp)* ;
        bool IsPrefix_ExpressionList(Token t)
        {
            return IsPrefix_Expression(t);
        }
        ExpressionListNode ParseExpressionList()
        {
            var list = Parse_Plus(IsPrefix_Expression, ParseExpression, IsComma);
            var expList = ConvertNodeList<ExpressionNode>(list);
            var node = new ExpressionListNode(expList);
            return node;
        }

        // tableconstructor : '{' (fieldlist)? '}' ;
        bool IsPrefix_TableConstructor(Token t)
        {
            return t.Type == TokenType.OpenBrace;
        }
        TableConstructorNode ParseTableConstructor()
        {
            Debug.Assert(CurrentToken.Type == TokenType.OpenBrace);
            ReadToken();
            FieldListNode fieldList = null;
            if (IsPrefix_FieldList(CurrentToken))
                fieldList = ParseFieldList();
            CurrentMustMatch(TokenType.CloseBrace);
            var node = new TableConstructorNode(fieldList);
            return node;
        }

        // fieldlist    : field(fieldsep field)* fieldsep? ;
        bool IsPrefix_FieldList(Token t)
        {
            return IsPrefix_Field(t);
        }
        bool IsFieldSeperator(Token t)
        {
            return t.Type == TokenType.Comma || t.Type == TokenType.SemiColon;
        }
        FieldListNode ParseFieldList()
        {
            List<Node> list = Parse_Plus(IsPrefix_Field, ParseField, IsFieldSeperator);
            var fieldList = ConvertNodeList<FieldNode>(list);
            var node = new FieldListNode(fieldList);
            return node;
        }

       // field : '[' exp ']' '=' exp | Name '=' exp | exp ;
       bool IsPrefix_Field(Token t)
        {
            return (t.Type == TokenType.OpenSquareBracket
                || t.Type == TokenType.Name
                || IsPrefix_Expression(t));
        }
        FieldNode ParseField()
        {
            Debug.Assert(IsPrefix_Field(CurrentToken));
            if(CurrentToken.Type == TokenType.OpenSquareBracket)
            {
                ExpressionNode index = ParseExpression();
                CurrentMustMatch(TokenType.CloseSquareBracket);
                NextMustBe(TokenType.SingleEquals);
                ReadToken();
                ExpressionNode value = ParseExpression();
                var node = new FieldNode(index, value);
                return node;
            }
            else if(CurrentToken.Type == TokenType.Name)
            {
                Token name = CurrentToken;
                NextMustBe(TokenType.SingleEquals);
                ReadToken();
                ExpressionNode value = ParseExpression();
                var node = new FieldNode(name, value);
                return node;
            }
            else if(IsPrefix_Expression(CurrentToken))
            {
                ExpressionNode value = ParseExpression();
                var node = new FieldNode(value);
                return node;
            }
            else
            {
                Debug.Assert(null == "Bad case in ParseField");
                return null;
            }
        }

        // functioncall : varOrExp nameAndArgs+ ;
        FunctionCallStatementNode ParseFunctionCall(Node variableOrExpression)
        {
            var list = Parse_Plus(IsPrefix_NameAndArgs, ParseNameAndArgs, null);
            var nameAndArgsList = ConvertNodeList<NameAndArgsNode>(list);
            var node = new FunctionCallStatementNode(variableOrExpression, nameAndArgsList);
            return node;
        }

        bool IsComma(Token t)
        {
            return t.Type == TokenType.Comma;
        }

        AssignmentNode ParseAssignment(VariableNode firstVar)
        {
            var varList = new List<Node>();

            varList.Add(firstVar);
            // scan of "zero or more" because we already have the first one.
            varList = Parse_Star(IsPrefix_Variable, ParseVariable, IsComma);

            NextMustBe(TokenType.SingleEquals);
            ReadToken();

            var expList = Parse_Plus(IsPrefix_Expression, ParseExpression, IsComma);

            List<VariableNode> variableList = CastNodeList<VariableNode>(varList);
            List<ExpressionNode> expressionList = CastNodeList<ExpressionNode>(expList);
            var node = new AssignmentNode(variableList, expressionList);
            return node;
        }

        List<T> CastNodeList<T>(List<Node> nodeList)
            where T: Node
        {
            var list = new List<T>();
            foreach(Node t in nodeList)
                list.Add((T)t);
            return list;
        }

        // namelist : Name(',' Name)* ;
        private List<Token> ParseNameList(List<Token> list = null)
        {
            if(list == null)
                list = new List<Token>();

            CurrentMustMatch(TokenType.Name);
            list.Add(CurrentToken);
            ReadToken();
            while(CurrentToken.Type == TokenType.Comma)
            {
                ReadToken();
                CurrentMustMatch(TokenType.Name);
                list.Add(CurrentToken);
            }
            return list;
        }

        // Same, when the first name is already read.
        List<Token> ParseNameList(Token name)
        {
            var list = new List<Token>();
            list.Add(name);
            ReadToken();
            if(CurrentToken.Type == TokenType.Comma)
            {
                ReadToken();
                list = ParseNameList(list);
            }
            return list;
        }

        // recursive part of expression parsing:
        //    exp: unop exp | exp binop exp | ... lots of other things.
        // removing the left recursion with:
        //    exp: unaryOp exp | exp1 binaryOp exp
        //    exp1: --- see larger expression below...
        bool IsPrefix_Expression(Token t)
        {
            return IsUnaryOp(t) || IsPrefix_ExpressionOne(t);
        }
        ExpressionNode ParseExpression()
        {
            ExpressionNode exp = null;
            if (IsUnaryOp(CurrentToken))
            {
                Token unaryOp = CurrentToken;
                _tokenizer.NextToken();
                exp = ParseExpression();
                var node = new ExpressionNode(unaryOp, exp);
                return node;
            }

            exp = (ExpressionNode)ParseExpressionOne();
            if(IsBinaryOp(CurrentToken))
            {
                Token binaryOp = CurrentToken;
                var exp2 = ParseExpression();
                var node = new ExpressionNode(binaryOp, exp, exp2);
                return node;
            }
            return exp;
        }

        //    exp1:  'nil' | 'false' | 'true' | Number | String | '...' | functiondef | 
        //                  prefixexp | tableconstructor
        //    functiondef: 'function' funcbody
        //    tableconstructor: '{' fieldlist+ '}'
        //    prefixexp    ::= var | functioncall | '(' exp ')'
        //    functioncall ::=  prefixexp args | prefixexp ':' Name args
        bool IsPrefix_ExpressionOne(Token t)
        {
            return IsSimpleValue(t) || t.Type == TokenType.DotDotDot
                || t.Type == TokenType.Function || IsPrefix_PrefixExpression(t)
                || IsPrefix_TableConstructor(t);
        }
        Node ParseExpressionOne()
        {
            if (IsSimpleValue(CurrentToken) || CurrentToken.Type == TokenType.DotDotDot)
            {
                Token t = CurrentToken;
                ReadToken();
                return new ExpressionNode(t);
            }

            if (IsPrefix_PrefixExpression(CurrentToken))
            {
                return ParsePrefixExpression();
            }

            switch(CurrentToken.Type)
            {
            case TokenType.Function:
                return ParseFunctionDefinition();

            case TokenType.OpenBrace:
                return ParseTableConstructor();

            default:
                Debug.Assert(null == "Bad Case in ParseExpressionOne");
                return null;
            }
        }

        // functiondef  : 'function' funcbody ;
        FunctionDefinitionNode ParseFunctionDefinition()
        {
            CurrentMustMatch(TokenType.Function);
            ReadToken();
            return ParseFunctionDefinitionBody();
        }

        // funcbody  : '(' (parlist)? ')' block 'end' ;
        FunctionDefinitionNode ParseFunctionDefinitionBody()
        {
            ParameterListNode paramList = null;

            CurrentMustMatch(TokenType.OpenParen);
            ReadToken();
            if (IsPrefix_ParameterList(CurrentToken))
                paramList = ParseParameterList();
            CurrentMustMatch(TokenType.CloseParen);
            ReadToken();
            BlockNode block = ParseBlock();
            CurrentMustMatch(TokenType.End);
            ReadToken();
            var node = new FunctionDefinitionNode(paramList, block);
            return node;
        }

        bool IsPrefix_PrefixExpression(Token t)
        {
            return IsPrefix_VarOrExp(t);
        }
        PrefixExpressionNode ParsePrefixExpression()
        {
            Node varOrExp = ParseVarOrExp();
            var list = Parse_Star(IsPrefix_NameAndArgs, ParseNameAndArgs, null);
            var nameAndArgsList = ConvertNodeList<NameAndArgsNode>(list);
            var node = new PrefixExpressionNode(varOrExp, nameAndArgsList);
            return node;
        }

        bool IsPrefix_VarOrExp(Token t)
        {
            return t.Type == TokenType.Name || t.Type == TokenType.OpenParen;
        }
        Node ParseVarOrExp()
        {
            if(CurrentToken.Type == TokenType.OpenParen)
            {
                ReadToken();
                ExpressionNode exp = ParseExpression();
                NextMustBe(TokenType.CloseParen);
            }
            return ParseVariable();
        }


        // Helpers

        static bool IsSimpleValue(Token t)
        {
            return IsNumber(t) || IsString(t)
                || t.Type == TokenType.Nil
                || t.Type == TokenType.True || t.Type == TokenType.False;
        }

        static bool IsString(Token t)
        {
            return t.Type == TokenType.NormalString || t.Type == TokenType.CharString
                || t.Type == TokenType.LongString;
        }

        static bool IsNumber(Token t)
        {
            return t.Type == TokenType.Int || t.Type == TokenType.Float
                || t.Type == TokenType.Float || t.Type == TokenType.Hex
                || t.Type == TokenType.Exp;
        }

        static bool IsBinaryOp(Token t)
        {
            return IsBinaryAddSubOp(t) || IsBinaryBitWiseOp(t)
                || IsBinaryMuldivModOp(t) || IsCompareOp(t);
        }

        static bool IsBinaryMuldivModOp(Token t)
        {
            return t.Type == TokenType.Multiply || t.Type == TokenType.Divide
                || t.Type == TokenType.Modulo || t.Type == TokenType.FloorDivide;
        }

        static bool IsBinaryAddSubOp(Token t)
        {
            return t.Type == TokenType.Add || t.Type == TokenType.Subtract;
        }

        static bool IsBinaryBitWiseOp(Token t)
        {
            return t.Type == TokenType.BitAnd || t.Type == TokenType.BitOr
                || t.Type == TokenType.BitLeft || t.Type == TokenType.BitRight;
        }

        static bool IsBinaryRightAssoc(Token t)
        {
            return t.Type == TokenType.Power || t.Type == TokenType.DotDot;
        }

        static bool IsCompareOp(Token t)
        {
            return t.Type == TokenType.EqualEqual || t.Type == TokenType.NotEqual
                || t.Type == TokenType.Less || t.Type == TokenType.LessOrEqual
                || t.Type == TokenType.Greater || t.Type == TokenType.GreaterOrEqual;
        }

        static bool IsUnaryOp(Token t)
        {
            return t.Type == TokenType.Subtract || t.Type == TokenType.Not
                || t.Type == TokenType.Hash;
        }
    }
}
