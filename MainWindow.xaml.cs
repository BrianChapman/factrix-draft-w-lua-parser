﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Factrix
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainDataContext _mainDataContext;

        public MainWindow()
        {
            InitializeComponent();
            _mainDataContext = (MainDataContext)DataContext;
        }

        object _previousSelection;


        private void BalanceButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void AddButtonClick(object sender, RoutedEventArgs e)
        {
            IconBook iconBook = _mainDataContext.IconBook;
            var addWindow = new AddItemWindow();
            addWindow.ShowDialog();
            //addWindow.GetSelectedValues();
        }

        private void RemoveButtonClick(object sender, RoutedEventArgs e)
        {

        }

        // This closes the "detail view" on ESC.
        private void DataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (e.Key == Key.Escape)
                dg.SelectedIndex = -1;
        }

        // This allows clicking the row to close the detail view if it was open.
        // Allowing a sort of "toggle" the detail view with clicking.
        private void ItemRowButtonClick(object sender, RoutedEventArgs e)
        {
            var bt = (Button)sender;
            Debug.WriteLine(string.Format("Selected Cell Change:  was: {0}  is: {1}", _previousSelection == null ? "null" : "non-Null", _dataGrid.SelectedIndex));
            if (_previousSelection == bt)
            {
                _dataGrid.SelectedIndex = -1;
                _previousSelection = null;
                _dataGrid.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
            }
            else
            {
                _previousSelection = bt;
                _dataGrid.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.VisibleWhenSelected;
            }

        }

        private void DownButtonClick(object sender, RoutedEventArgs e)
        {
            MoveSelectedItem(+1);
        }

        private void UpButtonClick(object sender, RoutedEventArgs e)
        {
            MoveSelectedItem(-1);
        }

        private void MoveSelectedItem(int dir)
        {
            var iSrc = _dataGrid.SelectedIndex;
            if (iSrc == -1)
                return;

            var dc = (MainDataContext)DataContext;
            var pl = dc.ProductionList;
            var iDest = iSrc + dir;
            if (iDest == pl.Count || iDest < 0)
                return;

            var item = pl[iSrc];
            pl.RemoveAt(iSrc);
            pl.Insert(iDest, item);
            _dataGrid.SelectedIndex = iDest;
        }


    }
}
